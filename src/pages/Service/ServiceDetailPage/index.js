import React from "react";
import ServiceOwner from "../../../components/Service/ServiceOwner";
import ServiceDetail from "../../../components/Service/ServiceDetail";
import ServiceApi from "../../../models/Service";
import MiniBox from "../../../components/MiniBox";
import dayjs from "dayjs";
import styles from "./ServiceDetailPage.module.css";
import ImageApi from "../../../models/Image";
import svg_tag_1_grad from "../../../images/tags/light/tag_1_graduation.svg";
import svg_tag_2_port from "../../../images/tags/light/tag_2_portrait.svg";
import svg_tag_3_wedd from "../../../images/tags/light/tag_3_wedding.svg";
import svg_tag_4_even from "../../../images/tags/light/tag_4_event.svg";
import svg_tag_5_prew from "../../../images/tags/light/tag_5_prewedding.svg";
import svg_tag_6_arch from "../../../images/tags/light/tag_6_architech.svg";
import svg_tag_7_food from "../../../images/tags/light/tag_7_foodanddrink.svg";

const svg_tag = [
  svg_tag_1_grad,
  svg_tag_2_port,
  svg_tag_3_wedd,
  svg_tag_4_even,
  svg_tag_5_prew,
  svg_tag_6_arch,
  svg_tag_7_food
];
class ServiceDetailPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      photographer: null,
      description: undefined,
      booltags: {
        1: false,
        2: false,
        3: false,
        4: false,
        5: false,
        6: false,
        7: false
      }
    };
  }

  async componentDidMount() {
    const { id } = { ...this.state };
    try {
      const res = await ServiceApi.getDetail(id);
      console.log("Service detail: ", res);

      let booltagsLocal = this.state.booltags;
      res.data.tags.forEach(tagpair => {
        if (tagpair.id !== 8) {
          booltagsLocal[tagpair.id] = true;
        }
      });
      this.setState({
        ...this.state,
        ...res.data,
        booltags: booltagsLocal
      });
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    const { photographer, description, id, coverPhoto } = {
      ...this.state
    };
    const tagLocgic = this.state.booltags;
    return (
      <div style={{ textAlign: "center" }} className={styles["encapsulator"]}>
        <div id={styles["cover-container"]}>
          <img
            id={styles["cover"]}
            src={ImageApi.getCoverServiceSrc(coverPhoto)}
            alt=""
          />
        </div>
        <div id={styles["content-container"]}>
          <div className={styles["tag-container"]}>
            {Object.keys(tagLocgic).map(i => {
              return tagLocgic[i] ? (
                <img
                  src={svg_tag[i - 1]}
                  alt=""
                  className={styles["img-tag"]}
                />
              ) : null;
            })}
          </div>
          <h1 style={{ fontSize: "2rem" }}>{this.state.title}</h1>
          <div className={styles["minibox-container"]}>
            <MiniBox>{this.state.eventCount} ผู้ใช้</MiniBox>
            <MiniBox>
              <span>ครึ่งวัน {this.state.halfDayPrice || "-"} บาท</span>
              <span>เต็มวัน {this.state.fullDayPrice || "-"} บาท</span>
            </MiniBox>
            <MiniBox>
              <span>{dayjs(this.state.openDate).format("DD/MM/YYYY")}</span>
              <span>-</span>
              <span>{dayjs(this.state.closeDate).format("DD/MM/YYYY")}</span>
            </MiniBox>
            <MiniBox>{this.state.location || "ไม่ได้ระบุสถานที่"}</MiniBox>
          </div>
          {photographer ? <ServiceOwner user={photographer} /> : <p>Loading</p>}
          {description !== undefined ? (
            <ServiceDetail
              detail={description}
              serviceId={id}
              ownerId={this.state.photographer.id}
            />
          ) : (
            <p>Loading</p>
          )}
        </div>
      </div>
    );
  }
}

export default ServiceDetailPage;
