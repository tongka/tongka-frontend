import React from "react";
import styles from "./UserTypeSelector.module.css";
import svg_usertype_customer from "../../images/UserTypeCustomer.svg";
import svg_usertype_customer_not_select from "../../images/UserTypeCustomer_notfocus.svg";
import svg_usertype_photographer from "../../images/UserTypePhotographer.svg";
import svg_usertype_photographer_not_select from "../../images/UserTypePhotoGrapher_notfocus.svg";

const USER_TYPE = [
  {
    value: "customer",
    label: "ผู้ใช้ทั่วไป"
  },
  {
    value: "photographer",
    label: "ช่างภาพ"
  }
];

class UserTypeSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      realState: 0,
      usertype: USER_TYPE[0]
    };
    this.onSwitchToZero = this.onSwitchToZero.bind(this);
    this.onSwitchToOne = this.onSwitchToOne.bind(this);
    this.onSwitchToRealState = this.onSwitchToRealState.bind(this);
    this.onSwitchToTempZero = this.onSwitchToTempZero.bind(this);
    this.onSwitchToTempOne = this.onSwitchToTempOne.bind(this);
  }

  onSwitchToTempZero() {
    this.setState({
      usertype: USER_TYPE[0]
    });
  }
  onSwitchToTempOne() {
    this.setState({
      usertype: USER_TYPE[1]
    });
  }

  onSwitchToRealState() {
    this.setState({
      usertype: USER_TYPE[this.state.realState]
    });
  }

  onSwitchToZero() {
    this.setState({
      realState: 0,
      usertype: USER_TYPE[0]
    });
  }
  onSwitchToOne() {
    this.setState({
      realState: 1,
      usertype: USER_TYPE[1]
    });
  }

  render() {
    return (
      <div className={styles["container"]}>
        <p
          style={{
            fontSize: "18px",
            textAlign: "center",
            margin: "0px auto 0.5em auto"
          }}
        >
          ประเภทผู้ใช้
        </p>
        <div
          id={styles["selector-box"]}
          onMouseLeave={this.onSwitchToRealState}
        >
          {/* leftside customer */}
          <div>
            {this.state.usertype === USER_TYPE[0] ? (
              <>
                <img
                  className={styles["imageholder-left-shadow"]}
                  src={svg_usertype_customer}
                  alt=""
                  onClick={this.onSwitchToZero}
                />
              </>
            ) : (
              <>
                <img
                  className={styles["imageholder-left-noshadow"]}
                  src={svg_usertype_customer_not_select}
                  alt=""
                  onClick={this.onSwitchToZero}
                  onMouseEnter={this.onSwitchToTempZero}
                />
              </>
            )}
          </div>
          {/* rightside photographer */}
          <div>
            {this.state.usertype === USER_TYPE[0] ? (
              <>
                <img
                  className={styles["imageholder-right-noshadow"]}
                  src={svg_usertype_photographer_not_select}
                  alt=""
                  onClick={this.onSwitchToOne}
                  onMouseEnter={this.onSwitchToTempOne}
                />
              </>
            ) : (
              <>
                <img
                  className={styles["imageholder-right-shadow"]}
                  src={svg_usertype_photographer}
                  alt=""
                  onClick={this.onSwitchToOne}
                />
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default UserTypeSelector;
