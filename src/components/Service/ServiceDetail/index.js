import React from "react";
import styles from "./ServiceDetail.module.css";
import { Icon } from "antd";
import { Link } from "react-router-dom";
import store from "../../../models/redux/store";
import ServiceRequest from "../ServiceRequest";
import User from "../../../models/User";

class ServiceDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOther: this.props.ownerId !== store.getState().auth.user.id
    };
  }

  async componentDidMount() {
    const res = await User.getProfile();
    this.setState({
      user: { ...res.data }
    });
  }

  render() {
    // ownerId is another thing you can use from this.props but we comment it to prevent eslint samrt acting
    const { detail, serviceId, fullDayPrice, halfDayPrice } = { ...this.props };
    let serviceRequest;
    if (this.state.user) {
      const { type } = { ...this.state.user };
      serviceRequest =
        type === "customer" ? (
          <ServiceRequest
            serviceId={serviceId}
            fullDayPrice={fullDayPrice}
            halfDayPrice={halfDayPrice}
          />
        ) : null;
    }

    return (
      <div className={styles["box-detail"]}>
        <div className={styles["header"]}>
          <span>รายละเอียด</span>
          {!this.state.isOther ? (
            <>
              <div id={styles["edit-button"]}>
                <Link to={`/service/${serviceId}/edit`}>
                  <Icon type="edit" />
                </Link>
              </div>
            </>
          ) : null}
        </div>
        <div className={styles["description"]}>
          {detail ? detail : "ไม่มีรายละเอียด"}
        </div>
        <div>{serviceRequest}</div>
      </div>
    );
  }
}

export default ServiceDetail;
