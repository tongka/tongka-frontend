import React, { Component } from "react";

import SearchItemService from "./SearchItem/SearchItemService";
import SearchItemPhotographer from "./SearchItem/SearchItemPhotographer";

class SearchResult extends Component {
  state = {
    result: null
  };
  componentWillUpdate(nextProps, nextState) {
    if (this.state === nextState || this.props === nextProps) return;
  }

  render() {
    let items = null;
    const { data, type } = this.props;

    if (data) {
      if (type === "service") {
        items = data.map(e => {
          const {
            id,
            title,
            halfDayPrice,
            fullDayPrice,
            description,
            openDate,
            closeDate,
            photographer,
            location,
            tags,
            coverPhoto
          } = e;
          let booltags = {
            1: false,
            2: false,
            3: false,
            4: false,
            5: false,
            6: false,
            7: false
          };
          tags.forEach(tagpair => {
            if (tagpair.id !== 8) {
              booltags[tagpair.id] = true;
            }
          });
          return (
            <SearchItemService
              key={id}
              data={{
                title,
                coverPhoto,
                halfPrice: halfDayPrice,
                fullPrice: fullDayPrice,
                detail: description,
                startDate: openDate,
                endDate: closeDate,
                location: location,
                tags: tags,
                booltags: booltags,
                owner: `${photographer.firstname} ${photographer.lastname}`,
                id
              }}
            />
          );
        });
      } else if (type === "photographer") {
        items = data.map(e => {
          const { id } = e;
          return <SearchItemPhotographer key={id} data={e} />;
        });
      }
    }

    return <>{items}</>;
  }
}
export default SearchResult;
