import React, { Component } from "react";
import { Form } from "antd";
import Service from "../../../models/Service";
import style from "./MyService.module.css";

class ServiceHeader extends Component {
  state = {
    expand: false
  };

  fetchservice() {
    this.props.form.validateFields(async () => {
      let result = null;
      result = await Service.getService();
      console.log(result);
      if (result) this.props.onSearch(result.data);
    });
  }

  componentDidMount() {
    this.fetchservice();
  }
  render() {
    return (
      <Form style={{ margin: "0px auto 5px auto", maxWidth: "970px" }}>
        <div>
          <h1 className={style["page-title"]}>บริการของฉัน</h1>
        </div>
      </Form>
    );
  }
}
export default Form.create({ name: "My Service" })(ServiceHeader);
