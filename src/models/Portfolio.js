import api from "./api";

class Portfolio {
  constructor() {
    this.url = "/portfolio";
  }

  getPortfolio = ownerId => {
    return api.api_application.get(`${this.url}?userId=${ownerId}`);
  };

  uploadPortfolio = formData => {
    return api.api_multipart.post(this.url, formData);
  };

  deletePortfolio = photoId => {
    return api.api_application.delete(`${this.url}?photoId=${photoId}`);
  };
}

const PortfolioApi = new Portfolio();
export default PortfolioApi;
