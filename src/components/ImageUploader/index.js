import { Icon, message } from "antd";
import React from "react";

import Image from "../../models/Image";

import styles from "./ImageUploader.module.css";

class ImageUploader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      imageUrl: this.props.imageUrl
    };
    this.isLocalImage = this.props.localImage || false;
    this.uploadHandler = this.props.onUpload;
    this.beforeUpload = this.props.beforeUpload || this.beforeDefaultUpload;
    this.inputFileRef = React.createRef();
    this.imageDisplayRef = React.createRef();
    this.imageRetryCount = 0;
  }

  beforeDefaultUpload = function(file) {
    const isSupport = ["image/jpeg", "image/png", "image/tiff"].includes(
      file.type
    );
    if (!isSupport) {
      message.error("Not support file type");
    }
    const isLt5M = file.size / 1024 / 1024 < 10;
    if (!isLt5M) {
      message.error("Image must be smaller than 10MB!");
    }
    return isSupport && isLt5M;
  };

  handleChange = async e => {
    const files = e.target.files;
    if (files.length !== 1) {
      return;
    }
    if (!this.beforeUpload(files[0])) {
      return;
    }

    this.setState({ loading: true });

    const { result, filepath } = await this.uploadHandler(files[0]);
    if (result) {
      this.setState({
        imageUrl: this.isLocalImage ? filepath : Image.getSrc(filepath),
        loading: false
      });
      this.imageRetryCount = 0;
    } else {
      this.inputFileRef.current.value = null;
      this.setState({ loading: false });
    }
  };

  handleClick = e => {
    if (this.state.loading) {
      message.loading("Please wait while uploading");
      return;
    }
    this.inputFileRef.current.click();
  };

  handleLoadedimage = e => {
    this.setState({ loading: false });
  };

  handleReloadimage = e => {
    this.setState({ loading: true });
    const newURL = this.state.imageUrl.split("?", 1) + "?" + +new Date();
    if (this.imageRetryCount > 200) {
      this.setState({ imageUrl: null, loading: false });
      this.imageRetryCount++;
    } else {
      this.setState({ imageUrl: newURL });
      this.imageRetryCount++;
    }
  };

  render() {
    const uploadButton = (
      <div className={styles["certer-icon"]}>
        <Icon
          type={this.state.loading ? "loading" : "plus"}
          style={{
            margin: "0px auto",
            fontSize: "30px",
            textAlign: "center"
          }}
        />
        <p className={styles["ant-upload-text"]}>Upload</p>
      </div>
    );
    const imageUrl = this.state.imageUrl;
    const { width, height, imgWidth, imgHeight } = this.props;
    return (
      <div
        styles={{
          position: "absolute",
          minWidth: `${width || this.props.minWidth}`,
          minHeight: `${height || this.props.minHeight}`,
          maxWidth: `${width || this.props.maxWidth}`,
          maxHeight: `${height || this.props.maxHeight}`
        }}
      >
        <input
          type="file"
          id="file-browser"
          ref={this.inputFileRef}
          style={{ display: "none" }}
          onChange={this.handleChange}
          // onInputCapture={this.handleChange}
        />
        <div
          onClick={this.handleClick}
          className={styles["avatar-uploader"]}
          style={{
            minWidth: `${width || this.props.minWidth}`,
            minHeight: `${height || this.props.minHeight}`,
            maxWidth: `${width || this.props.maxWidth}`,
            maxHeight: `${height || this.props.maxHeight}`
          }}
        >
          {imageUrl ? (
            <img
              src={imageUrl}
              ref={this.imageDisplayRef}
              alt="avatar"
              style={{
                position: "absolute",
                zIndex: "1",
                objectFit: "cover",
                borderRadius: "5px",
                width: `${imgWidth || "100%"}`,
                height: `${imgHeight || "auto"}`,
                userSelect: "none"
              }}
              onError={this.handleReloadimage}
              onLoad={this.handleLoadedimage}
            />
          ) : null}
          <div
            id={styles["fader-white"]}
            style={{
              minWidth: `${width || this.props.minWidth}`,
              minHeight: `${height || this.props.minHeight}`,
              maxWidth: `${width || this.props.maxWidth}`,
              maxHeight: `${height || this.props.maxHeight}`
            }}
          />
          {uploadButton}
        </div>
      </div>
    );
  }
}

export default ImageUploader;
