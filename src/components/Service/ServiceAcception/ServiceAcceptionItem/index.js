import React, { Component } from "react";
import { Card, Row, Col, Button } from "antd";

class ServiceAcceptionItem extends React.Component {
  render() {
    return (
      <>
        <Card style={{ margin: "1em" }}>
          <Row style={{ maxWidth: "950px" }}>
            <Col md={24} lg={24}>
              <h2>Client Name</h2>

              <p>วันที่รับบริการ: 19/4/19</p>
              <p>ประเภท: ครึ่งวัน</p>
              <p>สถานที่: สยามพารากอน</p>
              <p>รายละเอียดเพิ่มเติม: </p>
              <p>lorem ipsum google a cat move over a quck brown duck</p>
              <Row type="flex" justify="end" align="bottom">
                <Col md={12} lg={12}>
                  <Button type="primary" htmlType="submit" block>
                    ยอมรับ
                  </Button>
                </Col>
                <Col md={12} lg={12}>
                  <Button type="danger" htmlType="submit" block>
                    ปฎิเสธ
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Card>
      </>
    );
  }
}

export default ServiceAcceptionItem;
