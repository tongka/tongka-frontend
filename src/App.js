import { Layout } from "antd";
import { Route, Switch } from "react-router-dom";
import React, { Component } from "react";

import AccountPage from "./pages/Account";
import CreateService from "./pages/Service/createService";
import EditProfilePage from "./pages/Profile/EditProfile";
import EditServicePage from "./pages/Service/editService";
import EventPage from "./pages/Event";
import GuestRoute from "./components/GuestRoute";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Navbar from "./components/Navbar";
import PrivateRoute from "./components/PrivateRoute";
import ProfilePage from "./pages/Profile";
import Register from "./pages/Register";
import Search from "./pages/Search";
import MyService from "./pages/MyService";
import ServiceDetailPage from "./pages/Service/ServiceDetailPage";

import "./App.css";

const { Content } = Layout;

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <div
          style={{
            position: "fixed",
            left: "0px",
            top: "0px",
            width: "100%",
            zIndex: "999"
          }}
        >
          <Navbar />
        </div>
        <Content
          style={{
            padding: "0 50px",
            position: "relative",
            top: "50px",
            minHeight: "calc(100vh - 50px)"
          }}
        >
          <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>
            <Switch>
              <Route exact path="/" component={Home} />
              <GuestRoute exact path="/login" component={Login} />
              <GuestRoute exact path="/register" component={Register} />
              <Route exact path="/search" component={Search} />
              <Route exact path="/myservice" component={MyService} />

              <PrivateRoute exact path="/account" component={AccountPage} />
              <PrivateRoute exact path="/profile" component={ProfilePage} />
              <PrivateRoute
                exact
                path="/service/create"
                component={CreateService}
              />
              <PrivateRoute
                path="/service/:id/edit"
                component={EditServicePage}
              />

              <Route path="/service/:id" component={ServiceDetailPage} />

              <PrivateRoute path="/event" component={EventPage} />

              <PrivateRoute
                exact
                path="/profile/edit"
                component={EditProfilePage}
              />

              <Route path="/profile/:id" component={ProfilePage} />
            </Switch>
          </div>
        </Content>
      </React.Fragment>
    );
  }
}

export default App;
