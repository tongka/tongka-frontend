import api from "./api";

class Service {
  constructor() {
    this.url = "/service";
  }

  create = rawData => {
    const data = {
      title: rawData.title,
      openDate: rawData.openDate,
      closeDate: rawData.closeDate,
      halfDayPrice: rawData.halfDayPrice,
      fullDayPrice: rawData.fullDayPrice,
      description: rawData.description,
      location: rawData.location,
      tags: rawData.tags
    };
    return api.api_application.post(`${this.url}/create`, data);
  };

  request = rawData => {
    // const data = {
    //   title: rawData.title,
    //   openDate: rawData.openDate,
    //   closeDate: rawData.closeDate,
    //   halfDayPrice: rawData.halfDayPrice,
    //   fullDayPrice: rawData.fullDayPrice,
    //   description: rawData.description,
    //   location: rawData.location,
    //   tags: rawData.tags
    // };
    console.log("Send data: ", rawData);
    return api.api_application.post(`${this.url}/request`, rawData);
  };

  search = q => {
    return api.api_application.get(`${this.url}/search`, { params: { q } });
  };

  getService = () => {
    return api.api_application.get(`/user/service`);
  };

  getDetail = id => {
    return api.api_application.get(`${this.url}/get`, { params: { id } });
  };

  getTag = () => {
    return api.api_application.get(`${this.url}/tags`);
  };

  update = rawData => {
    const filters = [
      "tags",
      "title",
      "location",
      "halfDayPrice",
      "fullDayPrice",
      "description",
      "openDate",
      "closeDate",
      "id"
    ];
    const data = {};
    if (rawData) {
      Object.keys(rawData).forEach(e => {
        if (filters.includes(e)) {
          Object.assign(data, { [e]: rawData[e] });
        }
      });
    }
    console.log("data: ", data);
    return api.api_application.post(`${this.url}/edit`, data);
  };

  uploadCoverPhoto = data => {
    return api.api_multipart.post(`${this.url}/photo`, data);
  };
}

const ServiceApi = new Service();
export default ServiceApi;
