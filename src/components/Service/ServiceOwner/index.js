import React from "react";
import styles from "./ServiceOwner.module.css";
import { Link } from "react-router-dom";
import { Row, Col, Icon, Rate } from "antd";

import icon_fb from "../../../images/icons/social_icon_facebook.svg";
import icon_ig from "../../../images/icons/social_icon_ig.svg";
import icon_line from "../../../images/icons/social_icon_line.svg";
import icon_tel from "../../../images/icons/social_icon_tel.svg";
import ImageApi from "../../../models/Image";

const profileImgLayout = {
  md: {
    span: 24
  },
  lg: {
    span: 6
  }
};

const profileDetailLayout = {
  md: {
    span: 24
  },
  lg: {
    span: 18
  }
};

const profileDescLayout = {
  xs: {
    span: 24
  },
  md: {
    span: 12
  }
};

class ServiceOwner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(props, state) {
    if (JSON.stringify(state) !== JSON.stringify(props.user)) {
      return { ...props.user };
    }
    return null;
  }

  render() {
    const {
      firstname,
      lastname,
      email,
      eventCount,
      facebook,
      lineId,
      instagram,
      phone,
      reviewCount,
      rating,
      bio,
      profilePhoto,
      id
    } = { ...this.state };
    console.log("state: ", this.state);
    if (this.state !== {}) {
      return (
        <div className={styles["area-container"]}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="100%"
            height="1"
            version="1.1"
          >
            <rect width="100%" height="1" fill="#E0E0E0" />
          </svg>
          <Row id={styles["area-container-antdrow"]}>
            <Col {...profileImgLayout}>
              <figure className={styles["avatar"]}>
                <img
                  src={ImageApi.getProfileSrc(profilePhoto)}
                  alt="avatar"
                  className={styles["avatar-img"]}
                />
              </figure>
            </Col>
            <Col {...profileDetailLayout} className={styles["profile-detail"]}>
              <Row className={styles["owner-name"]}>
                <Link to={`/profile/${id}`}>
                  {firstname} {lastname}
                </Link>
              </Row>
              <Row className={styles["owner-detail-container"]}>
                <Col {...profileDescLayout} style={{ height: "100%" }}>
                  <div className={styles["owner-item-line-container"]}>
                    <div className={styles["owner-item-line"]}>
                      <Rate disabled defaultValue={Math.round(rating)} />{" "}
                      {reviewCount || "-"} reviews
                    </div>
                    <div className={styles["owner-item-line"]}>
                      {eventCount} งาน
                    </div>
                    <div className={styles["owner-bio"]}>{bio}</div>
                    <div className={styles["owner-item-line"]}>
                      <a href={`mail:${email}`}>
                        <Icon type="mail" /> {email || "-"}
                      </a>
                    </div>
                  </div>
                </Col>
                <Col
                  {...profileDescLayout}
                  className={styles["social-detail-container-antd"]}
                >
                  <div className={styles["social-row-container"]}>
                    <div className={styles["social-col-container"]}>
                      <div>
                        <a href={`https://www.fb.com/${facebook}`}>
                          <img
                            className={styles["social-icon"]}
                            id={styles["cover"]}
                            src={icon_fb}
                            alt=""
                            style={{ width: "25px", height: "25px" }}
                          />
                          {facebook || "-"}
                        </a>
                      </div>
                      <div>
                        <a href={`tel:${phone}`}>
                          <img
                            className={styles["social-icon"]}
                            id={styles["cover"]}
                            src={icon_tel}
                            alt=""
                            style={{ width: "25px", height: "25px" }}
                          />
                          {phone || "-"}
                        </a>
                      </div>
                    </div>
                    <div className={styles["social-col-container"]}>
                      <div>
                        <a href={`http://line.me/ti/p/~${lineId}`}>
                          <img
                            className={styles["social-icon"]}
                            id={styles["cover"]}
                            src={icon_line}
                            alt=""
                            style={{ width: "25px", height: "25px" }}
                          />
                          {lineId || "-"}
                        </a>
                      </div>
                      <div>
                        <a href={`http://instagram.com/${instagram}`}>
                          <img
                            className={styles["social-icon"]}
                            id={styles["cover"]}
                            src={icon_ig}
                            alt=""
                            style={{ width: "25px", height: "25px" }}
                          />
                          {instagram || "-"}
                        </a>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="100%"
            height="1"
            version="1.1"
          >
            <rect width="100%" height="1" fill="#E0E0E0" />
          </svg>
        </div>
      );
    }
    return <React.Fragment>Loading data..</React.Fragment>;
  }
}

export default ServiceOwner;
