import api from "./api";
import avatarImg from "../images/avatar.png";
import coverImg from "../images/Cover1.jpg";

class Image {
  constructor() {
    this.url = "/images";
    this.baseApi = api.getBaseURL();
  }

  getSrc = filepath => {
    if (!filepath) {
      // TODO: Return default photo (Please beware !!)
      return null;
    }
    return this.baseApi + filepath;
  };

  getProfileSrc = profilePhoto => {
    if (!profilePhoto) {
      return avatarImg;
    }
    return this.getSrc(profilePhoto.filepath);
  };

  getCoverSrc = coverPhoto => {
    if (!coverPhoto) {
      return coverImg;
    }
    return this.getSrc(coverPhoto.filepath);
  };

  getCoverServiceSrc = coverPhoto => {
    if (!coverPhoto) {
      return coverImg;
    }
    return this.getSrc(coverPhoto.filepath);
  };
}

const ImageApi = new Image();
export default ImageApi;
