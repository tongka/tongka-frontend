import React from "react";
import dayjs from "dayjs";
import { Button, Modal } from "antd";
import ImageApi from "../../../models/Image";
import styles from "./EventDetail.module.css";

class EventDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false
    };
    console.log(props);
  }

  getLabelType = type => {
    return type === "halfday" ? "ครึ่งวัน" : "เต็มวัน";
  };

  toggleModal = () => {
    this.setState({
      openModal: !this.state.openModal
    });
  };

  render() {
    const { eventData, stateName } = this.props;
    const totalPaymentSlipFilePath = eventData.totalSlip
      ? eventData.totalSlip.filepath
      : null;
    const redeemPaymentSlipFilePath = eventData.redeemSlip
      ? eventData.redeemSlip.filepath
      : null;
    const photosLink = this.props.eventData.photosLink
      ? !this.props.eventData.photosLink.match(/^[a-zA-Z]+:\/\//)
        ? "http://" + this.props.eventData.photosLink
        : this.props.eventData.photosLink
      : null;
    return (
      <div>
        <Button type="default" onClick={this.toggleModal}>
          ดูรายละเอียด
        </Button>
        <Modal
          visible={this.state.openModal}
          onCancel={this.toggleModal}
          footer={null}
          title={eventData.service.title}
        >
          <p>
            <b>สถานะ:</b> {stateName}
          </p>
          <p>
            <b>วันที่รับบริการ:</b>{" "}
            {dayjs(eventData.eventDate).format("DD/MM/YYYY")}
          </p>
          <p>
            <b>วันที่ขอบริการ:</b>{" "}
            {dayjs(eventData.requestDate).format("DD/MM/YYYY")}
          </p>
          {eventData.deliverDate ? (
            <p>
              <b>วันที่เสร็จสมบูรณ์:</b>{" "}
              {dayjs(eventData.deliverDate).format("DD/MM/YYYY")}
            </p>
          ) : null}
          <p>
            <b>ประเภท:</b> {this.getLabelType(eventData.type)}
          </p>
          <p>
            <b>สถานที่:</b> {eventData.location}
          </p>
          <p>
            <b>รายละเอียด:</b> {eventData.description}
          </p>
          <p>
            <b>ชำระเงินไปแล้ว:</b> {eventData.paidAmount} บาท
          </p>
          {photosLink ? (
            <>
              <p>
                <b>ที่อยู่รูปภาพ:</b> <a href={photosLink}>{photosLink}</a>
              </p>
            </>
          ) : null}
          {eventData.depositPaymentMethod ? (
            eventData.depositPaymentMethod === "bank account" ? (
              <>
                <p>
                  <b>ชำระมัดจำผ่านทาง:</b> ธนาคาร
                </p>
                <p>
                  <b>หลักฐานการชำระมัดจำ:</b>{" "}
                </p>
                <img
                  src={ImageApi.getSrc(eventData.depositSlip.filepath)}
                  alt=""
                  className={styles["slip-image"]}
                />
              </>
            ) : (
              <p>
                <b>ชำระมัดจำผ่านทาง:</b> บัตรเครดิต
              </p>
            )
          ) : null}
          {eventData.totalPaymentMethod ? (
            eventData.totalPaymentMethod === "bank account" ? (
              <>
                <p>
                  <b>ชำระเงินเต็มจำนวนผ่านทาง:</b> ธนาคาร
                </p>
                <p>
                  <b>หลักฐานการชำระเงินเต็มจำนวน:</b>{" "}
                </p>
                <img
                  src={ImageApi.getSrc(totalPaymentSlipFilePath)}
                  alt="ไม่มีรูปภาพ"
                  className={styles["slip-image"]}
                />
              </>
            ) : (
              <p>
                <b>ชำระเงินเต็มจำนวนผ่านทาง:</b> บัตรเครดิต
              </p>
            )
          ) : null}
          {eventData.redeemPaymentMethod ? (
            eventData.redeemPaymentMethod === "bank account" ? (
              <>
                <p>
                  <b>คืนเงินผ่านทาง:</b> ธนาคาร
                </p>
                <p>
                  <b>หลักฐานการคืนเงิน:</b>{" "}
                </p>
                <img
                  src={ImageApi.getSrc(redeemPaymentSlipFilePath)}
                  alt="ไม่มีรูปภาพ"
                  className={styles["slip-image"]}
                />
              </>
            ) : (
              <p>
                <b>คืนเงินผานทาง:</b> บัตรเครดิต
              </p>
            )
          ) : null}
        </Modal>
      </div>
    );
  }
}

export default EventDetail;
