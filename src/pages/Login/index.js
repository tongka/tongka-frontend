import React from "react";
import { Form, Icon, Input, Button, Row } from "antd";

import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { login } from "../../models/redux/actions/authAction";

class Login extends React.Component {
  componentDidMoute() {
    this.checkAuth();
  }
  componentDidUpdate() {
    this.checkAuth();
  }
  checkAuth = () => {
    if (this.props.auth.isAuth === true) {
      window.location.href = "/";
    }
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        this.props.login(values);
      } else {
        console.error(err.response);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <p>เข้าสู่ระบบ</p>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item>
            {getFieldDecorator("email", {
              rules: [{ required: true, message: "จำเป็นต้องใส่อีเมล์" }]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="อีเมล์"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("password", {
              rules: [{ required: true, message: "จำเป็นต้องใส่รหัสผ่าน" }]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder="รหัสผ่าน"
              />
            )}
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              block
            >
              เข้าสู่ระบบ
            </Button>
            <Row type="flex" justify="end">
              {/* TODO: Add forgot password */}
              <a className="login-form-forgot" href="/">
                ลืมรหัสผ่าน
              </a>
            </Row>
            <Row type="flex" justify="center">
              <p>หรือ</p>
            </Row>
            <Button type="default" block>
              <Link to="/register">สมัครสมาชิก</Link>
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { login }
)(Form.create({ name: "login" })(Login));
