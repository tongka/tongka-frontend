import React from "react";

import style from "./ClickableText.module.css";

class ClickableText extends React.PureComponent {
  render() {
    return (
      <button className={style["btn"]} onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}

export default ClickableText;
