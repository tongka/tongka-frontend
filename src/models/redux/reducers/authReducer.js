import { SET_CURRENT_USER, REMOVE_CURRENT_USER } from "../actions/types";

const initState = {
  isAuth: false,
  token: null,
  user: {}
};

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuth: true,
        token: action.token,
        user: action.user
      };
    case REMOVE_CURRENT_USER:
      return { ...initState };

    default:
      return state;
  }
};

export default authReducer;
