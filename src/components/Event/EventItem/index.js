import React from "react";
import { Button, Popconfirm, Popover, Input, Icon, message, Rate } from "antd";
import EventDetail from "../EventDetail";
import moment from "moment";
import styles from "./EventItem.module.css";
import { Link } from "react-router-dom";
import store from "../../../models/redux/store";
import EventApi from "../../../models/Event";
import PaymentConfirmation from "../../PaymentConfirmation";
import ImageApi from "../../../models/Image";

const verticalLine = (
  <svg
    width="1"
    height="100%"
    viewBox="0 0 1 150"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    className={styles["vertical-line"]}
  >
    <path d="M0.5 0.5V52.25V150" stroke="#E0E0E0" />
  </svg>
);

class EventItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSendImagePopVis: false
    };
    this.currentUser = store.getState().auth.user;
    this.stateNo = this.props.eventData.state;
    this.eventId = this.props.eventData.id;
    this.isPhotographerOwner =
      this.currentUser.id === this.props.eventData.service.photographer.id;
    this.isCustomerOwner =
      this.currentUser.id === this.props.eventData.customer.id;
    this.handleAnEventDeleted = this.props.onDeleteEvent;
    this.handleAnEventUpdated = this.props.onUpdateEvent;
    this.sendImageUrlRef = React.createRef();
  }

  handleClickCancel = async e => {
    if (this.stateNo === 0) {
      try {
        const res_confirm = await EventApi.confirm(String(this.eventId), "0");
        if (res_confirm.status === 200) {
          this.handleAnEventDeleted(this.eventId, this.stateNo);
        }
      } catch (err) {
        console.log(err);
      }
    } else if (this.stateNo === 1 || this.stateNo === 2 || this.stateNo === 3) {
      try {
        const res_cancel = await EventApi.cancel(String(this.eventId));
        if (res_cancel.status === 200) {
          let updatedEvent = {
            ...this.props.eventData,
            state: this.isCustomerOwner || this.stateNo === 1 ? 8 : 7
          };
          this.handleAnEventUpdated(
            this.eventId,
            updatedEvent,
            this.stateNo,
            updatedEvent.state === 8 ? 11 : 1
          );
        }
      } catch (err) {
        console.log(err);
      }
    } else {
      alert("You cannot cancel at this state");
    }
  };

  handleClickConfirm = async e => {
    if (this.stateNo === 0) {
      try {
        const res_confirm = await EventApi.confirm(String(this.eventId), "1");
        if (res_confirm.status === 200) {
          let updatedEvent = { ...this.props.eventData, state: 1 };
          this.handleAnEventUpdated(
            this.eventId,
            updatedEvent,
            this.stateNo,
            1
          ); //go to state 1
        }
      } catch (err) {
        console.log(err);
      }
    } else if (this.stateNo === 2 || this.stateNo === 3) {
    } else {
      alert("You cannot cancel at this state");
    }
  };

  handleCustomerPay = async (selectedPayType, imageSlipFile) => {
    if (this.stateNo === 1 /**ชำระมัดจำ*/) {
      const formData = new FormData();
      if (selectedPayType === "bank account") {
        formData.append("slipFile", imageSlipFile);
      }
      formData.append("id", this.eventId);
      formData.append("method", selectedPayType);
      message.loading("กำลังชำระมัดจำ");
      try {
        const payres = await EventApi.pay_30(formData);
        if (payres.status === 200) {
          let updatedEvent = { ...this.props.eventData, ...payres.data };
          this.handleAnEventUpdated(
            this.eventId,
            updatedEvent,
            this.stateNo,
            updatedEvent.state
          );
        }
      } catch (err) {
        message.error("ชำระมัดจำล้มเหลว");
        console.log(err);
        return false;
      }
    } else if (this.stateNo === 4 /**ชำระเงิน*/) {
      const formData = new FormData();
      if (selectedPayType === "bank account") {
        formData.append("slipFile", imageSlipFile);
      }
      formData.append("id", this.eventId);
      formData.append("method", selectedPayType);
      message.loading("กำลังชำระมัดจำ");
      try {
        const payres = await EventApi.pay_100(formData);
        if (payres.status === 200) {
          let updatedEvent = { ...this.props.eventData, ...payres.data };
          this.handleAnEventUpdated(
            this.eventId,
            updatedEvent,
            this.stateNo,
            updatedEvent.state
          );
        }
      } catch (err) {
        message.error("ชำระมัดเงินเหลว");
        console.log(err);
        return false;
      }
    } else {
      alert("Something went wrong");
      return false;
    }
    message.success("ชำระเรียบร้อย");
    return true;
  };

  handlePhotographerPay = async (selectedPayType, imageSlipFile) => {
    if (this.stateNo === 7 /**คืนเงิน */) {
      const formData = new FormData();
      if (selectedPayType === "bank account") {
        formData.append("slipFile", imageSlipFile);
      }
      formData.append("id", this.eventId);
      formData.append("method", selectedPayType);
      message.loading("กำลังชำระมัดจำ");
      try {
        const payres = await EventApi.pay_redeem(formData);
        if (payres.status === 200) {
          let updatedEvent = { ...this.props.eventData, ...payres.data };
          this.handleAnEventUpdated(this.eventId, updatedEvent, 1, 2);
        }
      } catch (err) {
        message.error("คืนมัดจำล้มเหลว");
        console.log(err);
        return false;
      }
    } else {
      alert("Something went wrong");
      return false;
    }
    message.success("คืนเงินเรียบร้อย");
    return true;
  };

  handlePhotographerConfirmPay = async () => {
    if (this.stateNo === 2 /**ยืนยันชำระมัดจำ*/) {
      try {
        const res = await EventApi.accept_30(this.eventId);
        if (res.status === 200) {
          let updatedEvent = this.props.eventData;
          updatedEvent.state = 3;
          this.handleAnEventUpdated(
            this.eventId,
            updatedEvent,
            this.stateNo,
            3
          );
        }
      } catch (err) {
        message.error("ยืนยันชำระมัดจำล้มเหลว");
        console.log(err);
        return false;
      }
    } else if (this.stateNo === 5 /**ยืนยันชำระเงิน*/) {
      try {
        const res = await EventApi.accept_100(this.eventId);
        if (res.status === 200) {
          let updatedEvent = this.props.eventData;
          updatedEvent.state = 6;
          this.handleAnEventUpdated(
            this.eventId,
            updatedEvent,
            this.stateNo,
            11
          );
        }
      } catch (err) {
        message.error("ยืนยันชำระเงินล้มเหลว");
        console.log(err);
        return false;
      }
    } else {
      alert("Something went wrong");
      return false;
    }
    message.success("ยืนยันชำระเงินเรียบร้อย");
    return true;
  };

  handleCustomerConfirmPay = async () => {
    if (this.stateNo === 9 /**ยืนยันคืนเงิน*/) {
      try {
        const res = await EventApi.accept_redeem(this.eventId);
        if (res.status === 200) {
          let updatedEvent = this.props.eventData;
          updatedEvent.state = 10;
          this.handleAnEventUpdated(this.eventId, updatedEvent, 2, 11);
        }
      } catch (err) {
        message.error("ยืนยันคืนเงินล้มเหลว");
        console.log(err);
        return false;
      }
    } else {
      alert("Something went wrong");
      return false;
    }
    message.success("ยืนยันคืนเงินเรียบร้อย");
    return true;
  };

  handleClickSendImage = async () => {
    try {
      const res = await EventApi.deliver_photo(
        this.eventId,
        this.sendImageUrlRef.current.input.value
      );
      if (res.status === 200) {
        let updatedEvent = { ...this.props.eventData, ...res.data };
        this.handleAnEventUpdated(
          this.eventId,
          updatedEvent,
          this.stateNo,
          updatedEvent.state
        );
      }
    } catch (err) {
      message.error("ส่งรูปภาพสำเร็จ");
      console.log(err);
      return false;
    }
    message.success("ส่งรูปภาพสำเร็จ");
  };

  handleHideSendImagePop = () => {
    this.setState({ isSendImagePopVis: false });
  };

  handleSendImagePopVisChange = visible => {
    this.setState({ isSendImagePopVis: visible });
  };

  handleConfirmDeliveredPhoto = async () => {
    alert("Accepted Delivered photo");
    try {
      const res = await EventApi.acc(
        this.eventId,
        this.sendImageUrlRef.current.input.value
      );
      if (res.status === 200) {
        let updatedEvent = { ...this.props.eventData, ...res.data };
        this.handleAnEventUpdated(
          this.eventId,
          updatedEvent,
          this.stateNo,
          updatedEvent.state
        );
      }
    } catch (err) {
      message.error("ส่งรูปภาพล้มเหลว");
      console.log(err);
      return false;
    }
    message.success("ส่งรูปภาพสำเร็จ");
  };

  handleChangeRate = async starVal => {
    EventApi.rate(this.eventId, starVal).then(() => {
      message.info("ให้คะแนนช่างภาพแล้ว " + starVal + " ดาว");
    });
  };

  render() {
    const { price, location, service, description } = this.props.eventData;
    let { stateName } = this.props;
    const eventDate = moment(this.props.eventData.eventDate).format(
      "DD/MM/YYYY"
    );
    const requestDate = moment(this.props.eventData.requestDate).format(
      "DD/MM/YYYY"
    );
    const eventOwner = this.isCustomerOwner
      ? service.photographer
      : this.props.eventData.customer;
    const type =
      this.props.eventData.type === "halfday" ? "ครึ่งวัน" : "เต็มวัน";
    const photosLink = this.props.eventData.photosLink
      ? !this.props.eventData.photosLink.match(/^[a-zA-Z]+:\/\//)
        ? "http://" + this.props.eventData.photosLink
        : this.props.eventData.photosLink
      : null;
    let finishedText = stateName;
    if (stateName === "เสร็จสมบูรณ์") {
      if (this.stateNo === 8) {
        finishedText = "ยกเลิกไม่มีการคืนเงิน";
      } else if (this.stateNo === 10) {
        finishedText = "ช่างภาพคืนเงินแล้ว";
      }
    }
    if (this.stateNo === 7) {
      stateName = "รอช่างภาพคืนเงิน";
    } else if (this.stateNo === 9) {
      stateName = "รอยืนยันคืนเงิน";
    }
    return (
      <div className={styles["event-item-container"]}>
        <div className={styles["service-detail"]}>
          <img
            id={styles["service-image"]}
            src={ImageApi.getCoverServiceSrc(service.coverPhoto)}
            alt="service cover"
          />
          <div className={styles["middle-flex-item"]}>
            <h2>
              <Link
                to={`/service/${service.id}`}
                className={styles["service-title"]}
              >
                {service.title}
              </Link>
            </h2>
            <p id={styles["event-description"]}>{description}</p>
          </div>
          {verticalLine}
        </div>
        <div className={styles["event-detail-container"]}>
          <div className={styles["event-detail"]}>
            <div
              className={styles["left-lable"]}
              textcontent={
                stateName === "เสร็จสมบูรณ์" ? finishedText : stateName
              }
            >
              สถานะ:{" "}
            </div>
            <div className={styles["left-lable"]} textcontent={eventDate}>
              วันที่รับบริการ:{" "}
            </div>
            <div
              className={[styles["left-lable"], styles["text-overflow"]].join(
                " "
              )}
              textcontent={`${eventOwner.firstname} ${eventOwner.lastname}`}
            >
              {this.isCustomerOwner ? "ผู้ให้บริการ:" : "ผู้รับบริการ:"}{" "}
            </div>
            <div className={styles["left-lable"]} textcontent={`${price} บาท`}>
              ค่าบริการ:{" "}
            </div>
          </div>
          <div className={styles["event-detail"]}>
            <div className={styles["left-lable"]} textcontent={type}>
              ประเภท:{" "}
            </div>
            <div
              className={[styles["left-lable"], styles["text-overflow"]].join(
                " "
              )}
              textcontent={location}
            >
              สถานที่:{" "}
            </div>
            <div className={styles["left-lable"]} textcontent={requestDate}>
              วันที่ขอรับบริการ:{" "}
            </div>
            {photosLink ? (
              <div
                className={styles["left-lable"]}
                // textcontent={`<a href=${photosLink}>${photosLink}</a>`}
              >
                ที่อยู่รูปภาพ: {<a href={photosLink}>คลิกที่นี่</a>}
              </div>
            ) : null}
          </div>

          <div className={styles["buttons-container"]}>
            <EventDetail
              eventData={this.props.eventData}
              stateName={stateName}
            />
            {this.stateNo === 0 && this.isPhotographerOwner ? (
              <Button
                type="primary"
                className={styles["button"]}
                onClick={this.handleClickConfirm}
              >
                ยืนยัน
              </Button>
            ) : null}
            {this.isCustomerOwner && [4, 5].includes(this.stateNo) ? (
              <Popover
                placement="topRight"
                content={
                  <>
                    <Rate onChange={this.handleChangeRate} />
                  </>
                }
                title="ให้คะนนช่างภาพ"
                trigger="click"
              >
                <Button type="default" className={styles["button"]}>
                  ให้ดาว
                </Button>
              </Popover>
            ) : null}
            {this.isCustomerOwner && [1, 2, 4, 5].includes(this.stateNo) ? (
              <PaymentConfirmation
                onPay={this.handleCustomerPay}
                isDisabled={[2, 5].includes(this.stateNo)}
                bankAccountNumber={this.currentUser.bankAccountNumber}
                isCreditCardAvailiable={this.currentUser.isCreditCardAvailiable}
                creditCardNumber={this.currentUser.creditCardNumber}
                cardExpireDate={this.currentUser.cardExpireDate}
                stateNo={this.stateNo}
                price={
                  [1, 2].includes(this.stateNo)
                    ? (price * 0.3).toFixed(2)
                    : (price * 0.7).toFixed(2)
                }
                isPhotographerOwner={this.isPhotographerOwner}
              />
            ) : null}
            {this.isPhotographerOwner && [7, 9].includes(this.stateNo) ? (
              <PaymentConfirmation
                onPay={this.handlePhotographerPay}
                isDisabled={this.stateNo === 9}
                bankAccountNumber={this.currentUser.bankAccountNumber}
                isCreditCardAvailiable={this.currentUser.isCreditCardAvailiable}
                creditCardNumber={this.currentUser.creditCardNumber}
                cardExpireDate={this.currentUser.cardExpireDate}
                stateNo={this.stateNo}
                price={(price * 0.3).toFixed(2)}
                isPhotographerOwner={this.isPhotographerOwner}
              />
            ) : null}
            {this.isPhotographerOwner && [2, 5].includes(this.stateNo) ? (
              <Popconfirm
                placement="topRight"
                title="ข้าพเจ้าได้ตรวจสอบหลักฐานแล้ว"
                onConfirm={this.handlePhotographerConfirmPay}
                okText="ใช่"
                cancelText="ไม่ใช่"
              >
                <Button type="primary" className={styles["button"]}>
                  ยืนยันชำระเงิน
                </Button>
              </Popconfirm>
            ) : null}
            {this.isCustomerOwner && this.stateNo === 9 ? (
              <Popconfirm
                placement="topRight"
                title="ข้าพเจ้าได้ตรวจสอบหลักฐานแล้ว"
                onConfirm={this.handleCustomerConfirmPay}
                okText="ใช่"
                cancelText="ไม่ใช่"
              >
                <Button type="primary" className={styles["button"]}>
                  ยืนยันคืนเงิน
                </Button>
              </Popconfirm>
            ) : null}
            {this.isPhotographerOwner && this.stateNo === 3 ? (
              <Popover
                content={
                  <React.Fragment>
                    <Input
                      placeholder="ที่อยู่ URL ของรูปภาพ"
                      allowClear
                      prefix={<Icon type="link" />}
                      ref={this.sendImageUrlRef}
                    />
                    <div className={styles["button--sendphoto-container"]}>
                      <Button
                        type="default"
                        className={styles["button"]}
                        onClick={this.handleHideSendImagePop}
                      >
                        กลับ
                      </Button>
                      <Button
                        type="primary"
                        className={styles["button"]}
                        onClick={this.handleClickSendImage}
                      >
                        ส่งภาพ
                      </Button>
                    </div>
                  </React.Fragment>
                }
                placement="topRight"
                title="ระบุ URL ของที่เก็บรูปภาพ"
                trigger="click"
                visible={this.state.isSendImagePopVis}
                onVisibleChange={this.handleSendImagePopVisChange}
              >
                <Button type="primary" className={styles["button"]}>
                  ส่งรูปภาพ
                </Button>
              </Popover>
            ) : null}
            {/* {this.isCustomerOwner && this.stateNo === 4 ? (
              <Popconfirm
                placement="topRight"
                title="ข้าพเจ้าได้ตรวจสอบรูปภาพแล้ว"
                onConfirm={this.handleConfirmDeliveredPhoto}
                okText="ใช่"
                cancelText="ไม่ใช่"
              >
                <Button type="primary" className={styles["button"]}>
                  ยืนยันได้รับรูปภาพ
                </Button>
              </Popconfirm>
            ) : null} */}
            {(this.isPhotographerOwner &&
              [0, 1, 2, 3].includes(this.stateNo)) ||
            (this.isCustomerOwner && [1, 2, 3].includes(this.stateNo)) ? (
              <>
                <Popconfirm
                  placement="topRight"
                  title="คุณค้องการที่จะยกเลิกใช่ไหม"
                  onConfirm={this.handleClickCancel}
                  okText="ใช่"
                  cancelText="ไม่ใช่"
                >
                  <Button type="danger" className={styles["button"]}>
                    ยกเลิก
                  </Button>
                </Popconfirm>
              </>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

export default EventItem;
