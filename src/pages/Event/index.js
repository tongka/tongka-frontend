import React from "react";
import { Tabs } from "antd";
import EventItem from "../../components/Event/EventItem";
import UserApi from "../../models/User";
import styles from "./Event.module.css";

const TabPane = Tabs.TabPane;

const status = {
  photographer: {
    0: "คำขอที่ได้รับ",
    1: "ชำระมัดจำ",
    2: "ยืนยันชำระมัดจำ",
    3: "ส่งรูป",
    4: "ชำระเงิน",
    5: "ยืนยันชำระเงิน",
    11: "เสร็จสมบูรณ์"
  },
  customer: {
    0: "การตอบรับ",
    1: "ชำระมัดจำ",
    2: "ยืนยันชำระมัดจำ",
    3: "ถ่ายรูป",
    4: "ชำระเงิน",
    5: "ยืนยันชำระเงิน",
    11: "เสร็จสมบูรณ์"
  }
};

class EventPage extends React.Component {
  constructor(props) {
    super(props);
    const userType = JSON.parse(localStorage.getItem("LoggedInUser")).type;
    this.state = {
      user: {
        type: userType
      },
      currentStatus: 0,
      events: {}
    };
    this.tabMenuRef = React.createRef();
    UserApi.getEvents().then(resEvent => {
      let modData = { ...resEvent.data };
      if (modData[7]) {
        if (!modData[1]) {
          modData[1] = [];
        }
        modData[1] = modData[7].concat(modData[1]);
      }
      if (modData[9]) {
        if (!modData[2]) {
          modData[2] = [];
        }
        modData[2] = modData[9].concat(modData[2]);
      }
      this.setState({ events: modData });
    });
    this.handleState = this.handleState.bind(this);
    this.handleAnEventDeleted = this.handleAnEventDeleted.bind(this);
    this.handleAnEventUpdated = this.handleAnEventUpdated.bind(this);
  }

  handleState = e => {
    this.setState({ ...this.state, currentStatus: e });
  };

  handleAnEventDeleted = (eventId, stateNumber) => {
    let localEvents = this.state.events;
    let eventInState = localEvents[stateNumber].filter(
      event => event.id !== eventId
    );
    localEvents[stateNumber] = eventInState;
    if (!eventInState[0]) {
      //empty array
      delete localEvents[stateNumber];
    }
    this.setState({ events: localEvents });
  };

  handleAnEventUpdated = (
    eventId,
    newEvent,
    stateNumberFrom,
    stateNumberTo
  ) => {
    this.handleAnEventDeleted(eventId, stateNumberFrom);
    let localEvents = this.state.events;
    if (!localEvents[stateNumberTo]) {
      localEvents[stateNumberTo] = [];
    }
    if (newEvent.state === 9) {
      localEvents[stateNumberTo].unshift(newEvent);
    } else {
      localEvents[stateNumberTo].push(newEvent);
    }
    this.setState({ events: localEvents, currentStatus: stateNumberTo });
    this.tabMenuRef.current.activeKey = String(stateNumberTo);
  };

  render() {
    return (
      <div
        style={{
          maxWidth: "850px",
          // minWidth: "340px",
          width: "70vw",
          margin: "1em auto 0px auto"
        }}
      >
        <h1 style={{ textAlign: "center" }}>งานของฉัน</h1>
        <div className={styles["tab--menu"]}>
          <Tabs
            defaultActiveKey="0"
            ref={this.tabMenuRef}
            activeKey={String(this.state.currentStatus)}
            onChange={this.handleState}
          >
            {Object.keys(status[this.state.user.type]).map(stateNo => {
              const state_events = this.state.events[stateNo];
              return (
                <TabPane
                  tab={status[this.state.user.type][stateNo]}
                  key={stateNo}
                >
                  {state_events ? (
                    state_events.map(event => {
                      return (
                        <EventItem
                          key={event.id}
                          stateName={status[this.state.user.type][stateNo]}
                          onDeleteEvent={this.handleAnEventDeleted}
                          onUpdateEvent={this.handleAnEventUpdated}
                          eventData={event}
                        />
                      );
                    })
                  ) : (
                    <>คุณไม่มีรายการใด ๆ อยู่ในสถาณะนี้</>
                  )}
                </TabPane>
              );
            })}
          </Tabs>
        </div>
      </div>
    );
  }
}

export default EventPage;
