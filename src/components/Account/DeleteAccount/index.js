import { connect } from "react-redux";
import { Input, Form, Button, Modal } from "antd";
import { withRouter } from "react-router-dom";
import React from "react";

import { logout } from "../../../models/redux/actions/authAction";
import UserApi from "../../../models/User";

import style from "./DeleteAccount.module.css";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 }
  }
};

class DeleteAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      confirmLoading: false
    };
  }

  toggleModal = () => {
    this.setState({
      ...this.state,
      isOpen: !this.state.isOpen,
      confirmLoading: false
    });
  };

  confirmDelete = async () => {
    this.setState({
      ...this.state,
      confirmLoading: true
    });

    let data = null;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        data = { ...values };
      }
    });

    if (data) {
      try {
        await UserApi.deleteAccount(data.confirmPassword);
        this.props.logout();
      } catch (e) {
        this.toggleModal();
        const err = e.response.data.reduce((out, val) => out.concat("\n", val));
        Modal.error({
          title: "Tongka: Error message",
          content: err
        });
      }
    } else {
      this.toggleModal();
    }
  };

  cancelDelete = () => {
    this.toggleModal();
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <h3 className={style["title"]}>Delete account</h3>
        <Form.Item {...formItemLayout} label="ยืนยันรหัสผ่าน">
          {getFieldDecorator("confirmPassword", {
            rules: [{ required: true, message: "กรุณายืนยันรหัสผ่าน" }]
          })(<Input type="password" />)}
          <Button type="danger" onClick={this.toggleModal}>
            ยืนยันการลบ
          </Button>
        </Form.Item>
        <Modal
          title="ยืนยันการลบบัญชีผู้ใช้"
          closable={false}
          visible={this.state.isOpen}
          onOk={this.confirmDelete}
          okText="ยืนยัน"
          okType="danger"
          onCancel={this.cancelDelete}
          cancelText="ยกเลิก"
          confirmLoading={this.state.confirmLoading}
        >
          <p>
            เมื่อยืนยันแล้วจะไม่สามารถกู้คืนบัญชีได้
            ต้องการยืนยันการลบบัญชีผู้ใช้ใช่หรือไม่
          </p>
        </Modal>
      </Form>
    );
  }
}

const DeleteAccountForm = Form.create({ name: "DeleteAccount" })(DeleteAccount);
export default withRouter(
  connect(
    null,
    { logout }
  )(DeleteAccountForm)
);
