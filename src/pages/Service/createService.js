import React from "react";
import dayjs from "dayjs";

import { Form, Input, DatePicker, Col, Button, Row, Select } from "antd";
import ServiceApi from "../../models/Service";

const { RangePicker } = DatePicker;
const Option = Select.Option;

class CreateService extends React.Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    TAGS: []
  };

  async componentDidMount() {
    const res = await ServiceApi.getTag();
    const TAGS = res.data.map(e => <Option key={e.id}>{e.tagName}</Option>);

    this.setState({
      ...this.state,
      TAGS
    });
  }

  handleSubmit = async e => {
    e.preventDefault();
    let rawData = null;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        rawData = {
          ...values,
          openDate: dayjs(values["range-picker"][0]["_d"]).format(),
          closeDate: dayjs(values["range-picker"][1]["_d"]).format()
        };
      }
    });
    console.log("Raw data: ", rawData);
    if (rawData) {
      try {
        const res = await ServiceApi.create(rawData);
        console.log(res);
        window.location.href = "/service";
      } catch (e) {
        const err = e.response.data.reduce((out, val) => out.concat("\n", val));
        alert(err);
      }
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { TAGS } = { ...this.state };
    const formItemLayout = {
      labelCol: {
        sm: { span: 24 },
        md: { span: 6 }
      },
      wrapperCol: {
        sm: { span: 24 },
        md: { span: 18 }
      }
    };

    const tailFormItemLayout = {
      wrapperCol: {
        sm: {
          span: 24,
          offset: 0
        },
        md: {
          span: 12,
          offset: 6
        }
      }
    };

    const rangeConfig = {
      rules: [{ type: "array", required: true, message: "Please select time!" }]
    };

    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Item {...formItemLayout} label="ประเภท">
          {getFieldDecorator("tags", {
            rules: [
              {
                required: true,
                message: "Please select your service type!"
              }
            ]
          })(
            <Select
              mode="tags"
              style={{ width: "100%" }}
              placeholder="Tags Mode"
            >
              {TAGS}
            </Select>
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="หัวข้อของบริการ">
          {getFieldDecorator("title", {
            rules: [
              {
                required: true,
                message: "Please input service name!"
              }
            ]
          })(
            <Input placeholder="ชื่อของบริการ เช่น ถ่ายรูป portrait คุณภาพ" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="สถานที่ให้บริการ">
          {getFieldDecorator("location", {
            rules: [
              {
                required: true,
                message: "Please input service area!"
              }
            ]
          })(
            <Input placeholder="สถานที่ เช่น กรุงเทพ และ ปริมนฑล, จุฬาลงกรณ์มหาวิทยาลัย" />
          )}
        </Form.Item>
        <Row gutter={24}>
          <Col sm={{ span: 24 }} md={{ span: 12 }}>
            <Form.Item
              {...formItemLayout}
              labelCol={{ md: { span: 12 } }}
              wrapperCol={{ md: { span: 12 } }}
              label="ราคาครึ่งวัน"
            >
              {getFieldDecorator("halfDayPrice", {
                rules: [
                  {
                    required: true,
                    message: "Please input half day price!"
                  }
                ]
              })(
                <Input placeholder="0.00" min={0.0} step={0.01} type="number" />
              )}
            </Form.Item>
          </Col>
          <Col sm={{ span: 24 }} md={{ span: 12 }}>
            <Form.Item
              {...formItemLayout}
              labelCol={{ md: { span: 12 } }}
              wrapperCol={{ md: { span: 12 } }}
              label="ราคาเต็มวัน"
            >
              {getFieldDecorator("fullDayPrice", {
                rules: [
                  {
                    required: true,
                    message: "Please input full day price!"
                  }
                ]
              })(
                <Input placeholder="0.00" min={0.0} step={0.01} type="number" />
              )}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item {...formItemLayout} label="ช่วงวันรับงาน">
          {getFieldDecorator("range-picker", rangeConfig)(
            <RangePicker format="DD/MM/YYYY" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="รายละเอียด">
          {getFieldDecorator("description")(
            <Input.TextArea autosize={{ minRows: 3, maxRows: 6 }} />
          )}
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" block>
            ประกาศบริการ
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default Form.create({ name: "createService" })(CreateService);
