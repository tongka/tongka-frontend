import React from "react";
import styles from "./MiniBox.module.css";

const MiniBox = props => {
  return (
    <div className={styles["detail-box"]}>
      <div className={styles["absolute-center"]}>{props.children}</div>
    </div>
  );
};

export default MiniBox;
