import React from "react";
import styles from "./PortfolioCard.module.css";
import { Popover, Icon, message } from "antd";
import ImageApi from "../../../models/Image";
import ClickableText from "../../ClickableText";

class PortfolioCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isClicked: false,
      imageSrc: ImageApi.getSrc(props.filepath)
    };
    this.isPhotographerOwner = props.isPhotographerOwner;
    this.handleDeletePort = props.onDeletePort;
    this.handleDelete = this.handleDelete.bind(this);
  }

  hide = () => {
    this.setState({
      isClicked: false
    });
  };

  handleVisibleChange = visible => {
    this.setState({ isClicked: visible });
    console.log(this.state);
  };

  handleDelete = () => {
    this.setState({ isClicked: false });
    this.handleDeletePort(this.props.id);
  };

  handleViewFullSize = () => {
    if (this.state.imageSrc) {
      window.location.href = this.state.imageSrc;
      this.setState({ isClicked: false });
    }
    message.error("there is no image");
  };

  render() {
    const { cardWidth, cardHeight } = this.props;
    return (
      <Popover
        content={
          <>
            <ClickableText onClick={this.handleViewFullSize}>
              view full size
            </ClickableText>
            <br />
            {this.isPhotographerOwner ? (
              <ClickableText onClick={this.handleDelete}>ลบ</ClickableText>
            ) : null}
          </>
        }
        placement="top"
        title={
          <>
            <span>{this.isPhotographerOwner ? "แก้ไข" : "เพิ่มเติม"}</span>{" "}
            <div id={styles["close-button"]} onClick={this.hide}>
              <Icon type="close" />
            </div>
          </>
        }
        trigger="click"
        visible={this.state.isClicked}
        onVisibleChange={this.handleVisibleChange}
      >
        <img
          src={this.state.imageSrc}
          width={cardWidth || 400}
          height={cardHeight || 300}
          alt=""
          className={styles["image"]}
        />
      </Popover>
    );
  }
}

export default PortfolioCard;
