import React from "react";
import {
  Form,
  Radio,
  Input,
  InputNumber,
  DatePicker,
  Button,
  message
} from "antd";
import moment from "moment";
import ServiceApi from "../../../models/Service";
import styles from "./ServiceRequest.module.css";

const RadioGroup = Radio.Group;

const lineBreaker = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="1"
    version="1.1"
    style={{ marginBottom: "1em", display: "block" }}
  >
    <rect width="100%" height="1" fill="#E0E0E0" />
  </svg>
);

class ServiceRequest extends React.Component {
  handleSubmit = async e => {
    // console.log("sd");
    e.preventDefault();
    let data = null;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        data = {
          ...values,
          type: values["day-type"],
          eventDate: moment(values.date).format(),
          serviceId: this.props.serviceId,
          price: String(values.price)
        };
      }
    });
    console.log(data);

    if (data) {
      try {
        const res = await ServiceApi.request(data);
        console.log(res);
        message.info("ส่งคำร้องขอเรียบร้อย");
        window.location.href = "/event";
      } catch (e) {
        const err = e.response.data.reduce((out, val) => out.concat("\n", val));
        console.log(err);
        alert(err);
      }
    }
  };

  render() {
    // console.log("sdds");
    const { getFieldDecorator } = this.props.form;
    const { halfDayPrice } = { ...this.props }; //fullDayPrice is removed

    const formItemLayout = {
      labelCol: {
        sm: { span: 24 },
        md: { span: 10 }
      },
      wrapperCol: {
        sm: { span: 24 },
        md: { span: 8 }
      }
    };

    // const tailFormItemLayout = {
    //   wrapperCol: {
    //     sm: {
    //       span: 24,
    //       offset: 0
    //     },
    //     md: {
    //       span: 12,
    //       offset: 6
    //     }
    //   }
    // };

    return (
      <>
        {lineBreaker}
        <p
          style={{
            textAlign: "center",
            fontSize: "24px",
            color: "#333",
            marginBottom: "0.5em"
          }}
        >
          สนใจรับบริการนี้
        </p>
        <div id={styles["request-form"]}>
          {/**style={{ marginRight: "15%" }} */}
          <Form onSubmit={this.handleSubmit}>
            <Form.Item
              {...formItemLayout}
              wrapperCol={{ sm: { span: 24 }, md: { span: 6 } }}
              label="วันที่"
              // className={styles["form-item-override"]}
            >
              {getFieldDecorator("date", {
                rules: [
                  {
                    required: true,
                    message: "กรุณาใส่วันที่"
                  }
                ]
              })(<DatePicker format="DD/MM/YYYY" />)}
            </Form.Item>

            <Form.Item
              {...formItemLayout}
              wrapperCol={{ sm: { span: 24 }, md: { span: 5 } }}
              label="ประเภท"
            >
              {getFieldDecorator("day-type", {
                rules: [
                  {
                    required: true,
                    message: "กรุณาเลือกประเภทของวัน"
                  }
                ]
              })(
                <RadioGroup name="radiogroup">
                  <Radio value={"halfday"}>ครึ่งวัน</Radio>
                  <Radio value={"fullday"}>เต็มวัน</Radio>
                </RadioGroup>
              )}
            </Form.Item>
            <div className={styles["request-form-input"]}>
              <Form.Item
                {...formItemLayout}
                wrapperCol={{ sm: { span: 24 }, md: { span: 8 } }}
                label="ราคา"
              >
                {getFieldDecorator("price", {
                  initialValue: `${halfDayPrice}`,
                  rules: [
                    {
                      required: true,
                      message: "กรุณาเสนอราคา"
                    }
                  ]
                })(
                  <InputNumber
                    min={0}
                    formatter={value => `฿ ${value}`}
                    parser={value => value.replace(/฿\s?|(,*)/g, "")}
                    size="100%"
                  />
                )}
              </Form.Item>
            </div>
            <div className={styles["request-form-input"]}>
              <Form.Item {...formItemLayout} label="สถานที่">
                {getFieldDecorator("location", {
                  rules: [
                    {
                      required: true,
                      message: "กรุณาเลือกสถานที่"
                    }
                  ]
                })(<Input placeholder="" />)}
              </Form.Item>
            </div>
            <div id={styles["request-form-input-text-area"]}>
              <Form.Item {...formItemLayout} label="รายละเอียดเพิ่มเติม">
                {getFieldDecorator("description", {
                  rules: [
                    {
                      required: true,
                      message: "กรุณาใส่รายละเอียด"
                    }
                  ]
                })(<Input.TextArea autosize={{ minRows: 3, maxRows: 6 }} />)}
              </Form.Item>
            </div>

            <Button type="primary" htmlType="submit">
              ร้องขอบริการ
            </Button>
          </Form>
        </div>
      </>
    );
  }
}

export default Form.create({ name: "ServiceRequest" })(ServiceRequest);
