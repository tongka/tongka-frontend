import React from "react";
import ServiceHeader from "../../components/Service/ServiceHeader";
import ServiceResult from "../../components/Service/ServiceResult";
import store from "../../models/redux/store";

class Myservice extends React.Component {
  constructor(props) {
    super(props);
    this.currentUser = store.getState().auth.user;
    this.getServiceData = this.getServiceData.bind(this);
  }

  state = {
    service_result: null
  };

  getServiceData = data => {
    Object.keys(data).map(e => {
      data[e].owner = `${this.currentUser.firstname} ${
        this.currentUser.lastname
      }`;
      return data;
    });
    this.setState({ service_result: data });
  };

  render() {
    return (
      <>
        <ServiceHeader onSearch={data => this.getServiceData(data)} />
        <ServiceResult data={this.state.service_result} />
      </>
    );
  }
}

export default Myservice;
