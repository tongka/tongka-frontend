import { Form, Input, DatePicker, Button, message, Radio } from "antd";
import moment from "moment";
import React from "react";

import ImageApi from "../../../models/Image";
import ImageUploader from "../../ImageUploader";
import store from "../../../models/redux/store";
import UserApi from "../../../models/User";

const RadioGroup = Radio.Group;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 10 }
  }
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    },
    sm: {
      span: 12,
      offset: 8
    }
  }
};

class EditUserProfile extends React.Component {
  constructor(props) {
    super(props);
    const storeProfilePhoto = store.getState().auth.user.profilePhoto;
    const storeCoverPhoto = store.getState().auth.user.coverPhoto;
    this.state = {
      initialProfilePicURL: storeProfilePhoto
        ? ImageApi.getSrc(storeProfilePhoto.filepath)
        : null,
      initialCoverURL: storeCoverPhoto
        ? ImageApi.getSrc(storeCoverPhoto.filepath)
        : null,
      paymentType: "bank account"
    };
  }

  handleUploadAvatar = async function(file) {
    const formData = new FormData();
    formData.append("imageFile", file);
    console.log("uploading..");
    console.log(formData);

    try {
      const res = await UserApi.uploadProfilePhoto(formData);
      console.log("upload avatar successful");
      console.log(res);
      UserApi.updateStoreUser(res.data);
      message.info("Profile picture saved!");
      return { result: true, filepath: res.data.profilePhoto.filepath };
    } catch (err) {
      console.log("Upload avatar error");
      console.log(err);
      message.error("Upload failed");
    }
    return { result: false, filepath: null };
  };

  handleUploadCover = async function(file) {
    const formData = new FormData();
    formData.append("imageFile", file);
    console.log("uploading..");
    console.log(formData);

    try {
      const res = await UserApi.uploadCoverPhoto(formData);
      console.log("upload cover successful");
      console.log(res);
      UserApi.updateStoreUser(res.data);
      message.info("Cover picture saved!");
      return { result: true, filepath: res.data.profilePhoto.filepath };
    } catch (err) {
      console.log("Upload cover error");
      console.log(err);
      message.error("Cover upload failed");
    }
    return { result: false, filepath: null };
  };

  handleSubmit = async e => {
    e.preventDefault();
    let data = null;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        data = { ...values, birthday: moment(values.birthday).format() };
      }
    });

    if (data) {
      try {
        const res = await UserApi.update(data);
        console.log(res);
        UserApi.updateStoreUser(res.data);
        window.location.href = "/profile";
      } catch (e) {
        const err = e.response.data.reduce((out, val) => out.concat("\n", val));
        alert(err);
      }
    }
  };

  handleOnPaymentTypeChange = e => {
    this.setState({ paymentType: e.target.value });
  };

  componentDidMount() {
    const fields = {};
    Object.keys(this.props.profile).forEach(e => {
      Object.assign(fields, { [e]: { value: this.props.profile[e] } });
    });
    fields["birthday"] = moment(fields["birthday"]);
    fields["cardExpireDate"] = moment(fields["cardExpireDate"]);
    console.log(fields);
    if (this.props.profile.paymentType) {
      this.setState({ paymentType: this.props.profile.paymentType });
    }
    this.props.form.setFields(fields);
  }

  handleSubmit = async e => {
    e.preventDefault();
    let data = null;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        data = {
          ...values,
          birthday: moment(values.birthday).format(),
          paymentType: this.state.paymentType,
          cardExpireDate: moment(values.cardExpireDate).format()
        };
      }
    });

    if (data) {
      try {
        const res = await UserApi.update(data);
        console.log(res);
        window.location.href = "/profile";
      } catch (e) {
        const err = e.response.data.reduce((out, val) => out.concat("\n", val));
        alert(err);
      }
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { paymentType } = this.state;

    return (
      <Form onSubmit={this.handleSubmit}>
        <div style={{ height: "232px" }}>
          <Form.Item {...formItemLayout} label="รูปภาพประจำตัว">
            <ImageUploader
              onUpload={this.handleUploadAvatar}
              imageUrl={this.state.initialProfilePicURL}
              width="200px"
              height="200px"
              imgHeight="200px"
              imgWidth="100%"
            />
          </Form.Item>
        </div>
        <div style={{ height: "532px" }}>
          <Form.Item {...formItemLayout} label="รูปภาพหน้าปก">
            <div
              style={{ height: "500px", maxWidth: "50vw", minWidth: "200px" }}
            >
              <ImageUploader
                onUpload={this.handleUploadCover}
                imageUrl={this.state.initialCoverURL}
                width="50vw"
                height="500px"
                imgHeight="500px"
                imgWidth="100%"
              />
            </div>
          </Form.Item>
        </div>
        <Form.Item {...formItemLayout} label="ชื่อ">
          {getFieldDecorator("firstname", {
            rules: [
              {
                required: true,
                message: "กรุณากรอกชื่อ"
              }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="นามสกุล">
          {getFieldDecorator("lastname", {
            rules: [
              {
                required: true,
                message: "กรุณากรอกนามสกุล"
              }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="วันเกิด">
          {getFieldDecorator("birthday", {
            initialValue: this.props.profile.birthday
              ? moment(this.props.profile.birthday)
              : moment()
          })(<DatePicker format="DD/MM/YYYY" />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="เบอร์โทรศัพท์">
          {getFieldDecorator("phone", {})(
            <Input placeholder="เช่น 08XXXXXXXX" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Facebook id">
          {getFieldDecorator("facebook", {})(
            <Input placeholder="เช่น fb.me/myfacebookid" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Instagram id">
          {getFieldDecorator("instagram", {})(
            <Input placeholder="เช่น myigid" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Line id">
          {getFieldDecorator("lineId", {})(
            <Input placeholder="เช่น mylineid" />
          )}
        </Form.Item>

        <Form.Item {...formItemLayout} label="เลขที่บัญชี">
          {getFieldDecorator("bankAccountNumber", {
            rules: [
              {
                required: true,
                message: "กรุณาระบุเลขที่บัญชี"
              }
            ],
            initialValue: this.props.profile.bankAccountNumber
          })(
            <Input placeholder="เลขที่บัญชีของท่าน เช่น 111-11122-23365-11" />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="ประเภทการชำระเงิน">
          {getFieldDecorator("paymentType", { initialValue: "bank account" })(
            <RadioGroup
              name="radiogroup_paymentType"
              onChange={this.handleOnPaymentTypeChange}
            >
              <Radio value={"bank account"}>โอนผ่านธนาคาร</Radio>
              <Radio value={"credit card"}>จ่ายผ่านบัตรเครดิต</Radio>
            </RadioGroup>
          )}
        </Form.Item>

        <Form.Item {...formItemLayout} label="เลขบัตรเครดิต">
          {getFieldDecorator("creditCardNumber", {
            rules: [
              {
                required: paymentType === "credit card",
                message: "กรุณาระบุเลขบัตรเครดิต"
              }
            ],
            initialValue: this.props.profile.creditCardNumber
          })(
            <Input
              placeholder="เลขบัตรเครดิตของท่าน เช่น 111-11122-23365-11"
              disabled={paymentType !== "credit card"}
            />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="วันที่บัตรหมดอายุ">
          {getFieldDecorator("cardExpireDate", {
            rules: [
              {
                required: paymentType === "credit card",
                message: "กรุณาระบุวันหมดอายุของบัตรเครดิต"
              }
            ],
            initialValue: this.props.profile.cardExpireDate
              ? moment(this.props.profile.cardExpireDate)
              : moment()
          })(
            <DatePicker
              format="DD/MM/YYYY"
              disabled={paymentType !== "credit card"}
            />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="เลข cvv">
          {getFieldDecorator("cvv", {
            rules: [
              {
                required: paymentType === "credit card",
                message: "กรุณาระบุเลข cvv"
              }
            ],
            initialValue: this.props.profile.cvv
          })(
            <Input
              placeholder="ตัวเลข cvv ด้านหลังบัตร"
              disabled={paymentType !== "credit card"}
            />
          )}
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            แก้ไข
          </Button>
          <Button
            type="secondary"
            onClick={() => (window.location.href = "/profile")}
          >
            ยกเลิก
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default Form.create({ name: "EditUserProfile" })(EditUserProfile);
