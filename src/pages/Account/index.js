import React from "react";
import style from "./Account.module.css";
import DeleteAccount from "../../components/Account/DeleteAccount";

class AccountPage extends React.Component {
  render() {
    return (
      <>
        <h2 className={style["page-title"]}>Manage account</h2>
        <DeleteAccount />
      </>
    );
  }
}

export default AccountPage;
