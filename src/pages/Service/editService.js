import React from "react";
import moment from "moment";

import {
  Form,
  Input,
  DatePicker,
  Col,
  Button,
  Row,
  Select,
  message
} from "antd";
import ServiceApi from "../../models/Service";
import store from "../../models/redux/store";
import ImageUploader from "../../components/ImageUploader";
import ImageApi from "../../models/Image";
import { Link } from "react-router-dom";

const { RangePicker } = DatePicker;
const Option = Select.Option;

class EditService extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOwner: null,
      confirmDirty: false,
      autoCompleteResult: [],
      TAGS: [],
      serviceId: this.props.match.params.id,
      initialCoverURL: null
    };
    console.log(this.state);
  }

  async componentDidMount() {
    const resgetTag = await ServiceApi.getTag();
    const TAGS = resgetTag.data.map(e => (
      <Option key={e.id}>{e.tagName}</Option>
    ));

    const res = await ServiceApi.getDetail(this.state.serviceId);
    this.setState({
      ...this.state,
      isOwner: res.data.photographer.id === store.getState().auth.user.id,
      initialCoverURL: res.data.coverPhoto
        ? ImageApi.getCoverServiceSrc(res.data.coverPhoto)
        : null,
      TAGS
    });
    let Used_TAGS = [];
    res.data.tags.forEach(e => {
      Used_TAGS.push(String(e.id));
    });
    const fields = {};
    Object.keys(res.data).forEach(e => {
      Object.assign(fields, { [e]: { value: res.data[e] } });
    });
    fields["tags"] = { value: Used_TAGS };
    fields["range-picker"] = {
      value: [moment(res.data.openDate), moment(res.data.closeDate)]
    };
    fields["halfDayPrice"] = { value: String(res.data.halfDayPrice) };
    fields["fullDayPrice"] = { value: String(res.data.fullDayPrice) };
    delete fields["available"];
    delete fields["closeDate"];
    delete fields["openDate"];
    delete fields["id"];

    this.props.form.setFields(fields);
  }

  handleSubmit = async e => {
    e.preventDefault();
    let rawData = null;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        rawData = {
          ...values,
          openDate: moment(values["range-picker"][0]["_d"]),
          closeDate: moment(values["range-picker"][1]["_d"]),
          id: this.state.serviceId
        };
      }
    });
    console.log("Raw data: ", rawData);
    if (rawData) {
      try {
        const res = await ServiceApi.update(rawData);
        console.log(res);
        window.location.href = `/service/${this.state.serviceId}`;
      } catch (e) {
        const err = e.response.data.reduce((out, val) => out.concat("\n", val));
        alert(err);
      }
    }
  };

  handleUploadCover = async function(file) {
    const formData = new FormData();
    console.log("upload serviceId:", this.props.serviceId);
    console.log("props: ", this.props);
    formData.append("id", this.props.serviceId);
    formData.append("imageFile", file);
    console.log("uploading..");
    console.log(formData);

    try {
      const res = await ServiceApi.uploadCoverPhoto(formData);
      console.log("upload cover successful");
      console.log(res);
      message.info("Cover picture saved!");
      return { result: true, filepath: res.data.coverPhoto.filepath };
    } catch (err) {
      console.log("Upload cover error");
      console.log(err);
      message.error("Cover upload failed");
    }
    return { result: false, filepath: null };
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { TAGS, isOwner } = { ...this.state };
    const formItemLayout = {
      labelCol: {
        sm: { span: 24 },
        md: { span: 6 }
      },
      wrapperCol: {
        sm: { span: 24 },
        md: { span: 18 }
      }
    };

    const tailFormItemLayout = {
      wrapperCol: {
        sm: {
          span: 24,
          offset: 0
        },
        md: {
          span: 12,
          offset: 6
        }
      }
    };

    const rangeConfig = {
      rules: [{ type: "array", required: true, message: "Please select time!" }]
    };

    if (isOwner === false) {
      this.props.history.push(`/service/${this.state.serviceId}`);
    }
    return (
      isOwner && (
        <Form onSubmit={this.handleSubmit}>
          <div style={{ height: "532px" }}>
            <Form.Item {...formItemLayout} label="รูปภาพหน้าปก">
              <div
                style={{ height: "500px", maxWidth: "50vw", minWidth: "200px" }}
              >
                <ImageUploader
                  serviceId={this.state.serviceId}
                  onUpload={this.handleUploadCover}
                  imageUrl={this.state.initialCoverURL}
                  width="50vw"
                  height="500px"
                  imgHeight="500px"
                  imgWidth="100%"
                />
              </div>
            </Form.Item>
          </div>
          <Form.Item {...formItemLayout} label="ประเภท">
            {getFieldDecorator("tags", {
              rules: [
                {
                  required: true,
                  message: "Please select your service type!"
                }
              ]
            })(
              <Select
                mode="tags"
                style={{ width: "100%" }}
                placeholder="Tags Mode"
              >
                {TAGS}
              </Select>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="หัวข้อของบริการ">
            {getFieldDecorator("title", {
              rules: [
                {
                  required: true,
                  message: "Please input service name!"
                }
              ]
            })(
              <Input placeholder="ชื่อของบริการ เช่น ถ่ายรูป portrait คุณภาพ" />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="สถานที่ให้บริการ">
            {getFieldDecorator("location", {
              rules: [
                {
                  required: true,
                  message: "Please input service area!"
                }
              ]
            })(
              <Input placeholder="สถานที่ เช่น กรุงเทพ และ ปริมนฑล, จุฬาลงกรณ์มหาวิทยาลัย" />
            )}
          </Form.Item>
          <Row gutter={24}>
            <Col sm={{ span: 24 }} md={{ span: 12 }}>
              <Form.Item
                {...formItemLayout}
                labelCol={{ md: { span: 12 } }}
                wrapperCol={{ md: { span: 12 } }}
                label="ราคาครึ่งวัน"
              >
                {getFieldDecorator("halfDayPrice", {
                  rules: [
                    {
                      required: true,
                      message: "Please input half day price!"
                    }
                  ]
                })(
                  <Input
                    placeholder="0.00"
                    min={0.0}
                    step={0.01}
                    type="number"
                  />
                )}
              </Form.Item>
            </Col>
            <Col sm={{ span: 24 }} md={{ span: 12 }}>
              <Form.Item
                {...formItemLayout}
                labelCol={{ md: { span: 12 } }}
                wrapperCol={{ md: { span: 12 } }}
                label="ราคาเต็มวัน"
              >
                {getFieldDecorator("fullDayPrice", {
                  rules: [
                    {
                      required: true,
                      message: "Please input full day price!"
                    }
                  ]
                })(
                  <Input
                    placeholder="0.00"
                    min={0.0}
                    step={0.01}
                    type="number"
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
          <Form.Item {...formItemLayout} label="ช่วงวันรับงาน">
            {getFieldDecorator("range-picker", rangeConfig)(
              <RangePicker format="DD/MM/YYYY" />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="รายละเอียด">
            {getFieldDecorator("description")(
              <Input.TextArea autosize={{ minRows: 3 }} />
            )}
          </Form.Item>

          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit" block>
              แก้ไขบริการ
            </Button>
            <Link to={`/service/${this.state.serviceId}`}>
              <Button type="default" block>
                ยกเลิก
              </Button>
            </Link>
          </Form.Item>
        </Form>
      )
    );
  }
}

export default Form.create({ name: "editService" })(EditService);
