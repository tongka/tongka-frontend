import jwt_decode from "jwt-decode";

import { SET_CURRENT_USER, REMOVE_CURRENT_USER } from "./types";
import UserApi from "../../User";
import api from "../../api";

// Login - Get User Token
export const login = userData => async dispatch => {
  try {
    const res = await UserApi.login(userData);
    const { accessToken } = res.data;

    localStorage.setItem("jwtToken", accessToken);
    api.setToken(); // set after set the value in localStorage

    const res2 = await UserApi.getProfile();
    const loggedInUser = res2.data;
    localStorage.setItem("LoggedInUser", JSON.stringify(loggedInUser));

    const decoded = jwt_decode(accessToken);

    dispatch(setCurrentUser(accessToken, decoded, loggedInUser));
  } catch (e) {
    const err = e.response.data.reduce((out, val) => out.concat("\n", val));
    alert(err);
  }
};
export const logout = () => dispatch => {
  localStorage.removeItem("jwtToken");
  localStorage.removeItem("LoggedInUser");
  api.setToken();

  dispatch({ type: REMOVE_CURRENT_USER });
};

// Set logged in user
export const setCurrentUser = (token, decoded, loggedInUser) => {
  return {
    type: SET_CURRENT_USER,
    token: token,
    user: {
      ...loggedInUser
    }
  };
};
