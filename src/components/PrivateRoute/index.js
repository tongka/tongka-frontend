import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

class PrivateRoute extends Component {
  render() {
    const { component: Component, auth, ...rest } = this.props;

    return (
      <Route
        {...rest}
        render={props =>
          auth.isAuth === true ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: "/login"
              }}
            />
          )
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(PrivateRoute)
);
