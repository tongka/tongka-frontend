import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { Menu, Dropdown, Icon, Drawer } from "antd";
import React from "react";

import { logout } from "../../models/redux/actions/authAction";
import ImageApi from "../../models/Image";

import avatar from "../../images/avatar.png";
import logo_navbar from "../../images/logo_navbar.svg";
import styles from "./Navbar.module.css";

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDrawerVis: false
    };
  }

  showDrawer = e => {
    e.preventDefault();
    this.setState({
      isDrawerVis: true
    });
  };

  handleDrawerClose = e => {
    e.preventDefault();
    this.setState({
      isDrawerVis: false
    });
  };

  render() {
    const { user } = this.props.auth;
    const userbutton = (
      <div id={styles["userbutton-menu"]}>
        <Menu>
          <Menu.Divider />
          <Menu.Item key="0-nav-bar-profile">
            <Link to="/profile">
              <span className={styles["userbutton-text"]}>หน้าผู้ใช้</span>
            </Link>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item key="1-nav-bar-account">
            <Link to="/account">จัดการบัญชี</Link>
          </Menu.Item>
          <Menu.Item key="2-nav-bar-logout">
            <a href="/" onClick={() => this.props.logout()}>
              ออกจากระบบ
            </a>
          </Menu.Item>
        </Menu>
      </div>
    );

    return (
      <div className={styles.nav}>
        <div className={styles["nav-header"]}>
          <div className={styles["nav-title"]}>
            <Link to="/">
              <span id={styles["nav-logo"]}>
                {" "}
                <span>
                  <Icon
                    type="bars"
                    onClick={this.showDrawer}
                    id={styles["bar-icon"]}
                  />
                  <Drawer
                    title={
                      <Link to="/">
                        <span id={styles["nav-logo-mini"]}>
                          {" "}
                          <img
                            src={logo_navbar}
                            alt=""
                          /> <span>TONGKA</span>{" "}
                        </span>
                      </Link>
                    }
                    placement="left"
                    closable={false}
                    onClose={this.handleDrawerClose}
                    visible={this.state.isDrawerVis}
                    width="250px"
                  >
                    <div className={styles["nav-links-vertical"]}>
                      <Link to="/search">ค้นหา</Link>

                      {!this.props.auth.isAuth ? (
                        <React.Fragment>
                          <Link to="/login">เข้าสู่ระบบ</Link>
                          <Link to="/register">สมัครสมาชิก</Link>
                        </React.Fragment>
                      ) : (
                        <React.Fragment>
                          {user.type === "photographer" && (
                            <Link to="/service/create">สร้างบริการ</Link>
                          )}
                          {user.type === "photographer" && (
                            <Link to="/myservice">บริการของฉัน </Link>
                          )}
                          <Link to="/event">งานของฉัน</Link>
                          <div
                            style={{
                              borderTop: "1px solid #e8e8e8",
                              width: "100%"
                            }}
                          />
                          <span style={{ color: "white" }}>
                            <img
                              src={ImageApi.getProfileSrc(user.profilePhoto)}
                              alt={avatar}
                              style={{ borderRadius: "50%", width: "30px" }}
                            />
                            <Link to="/profile">
                              <span style={{ marginLeft: "0.5rem" }}>
                                หน้าผู้ใช้
                              </span>
                            </Link>
                          </span>
                          <Link to="/account">จัดการบัญชี</Link>
                          <a href="/" onClick={() => this.props.logout()}>
                            ออกจากระบบ
                          </a>
                        </React.Fragment>
                      )}
                    </div>
                  </Drawer>
                </span>
                <img src={logo_navbar} alt="" />
                <span>TONGKA</span>{" "}
              </span>
            </Link>
          </div>
        </div>
        {/* <div className={styles['nav-btn']}>
          <label for={styles['nav-check']}>
            <span />
            <span />
            <span />
          </label>
        </div> */}
        <input type="checkbox" id={styles["nav-check"]} />
        <div className={styles["nav-links"]}>
          <Link to="/search">ค้นหา</Link>
          {!this.props.auth.isAuth ? (
            <React.Fragment>
              <Link to="/login">เข้าสู่ระบบ</Link>
              <Link to="/register">สมัครสมาชิก</Link>
            </React.Fragment>
          ) : (
            <React.Fragment>
              {user.type === "photographer" && (
                <Link to="/service/create">สร้างบริการ</Link>
              )}
              {user.type === "photographer" && (
                <Link to="/myservice">บริการของฉัน </Link>
              )}
              <Link to="/event">งานของฉัน</Link>
              <span style={{ color: "white", margin: "10px" }}>
                <img
                  src={ImageApi.getProfileSrc(user.profilePhoto)}
                  alt={avatar}
                  style={{ borderRadius: "50%", width: "30px" }}
                />
                <Dropdown
                  overlay={userbutton}
                  trigger={["click"]}
                  overlayStyle={{ position: "fixed" }}
                >
                  <span className="ant-dropdown-link" href="#">
                    {/* onClick={() => console.log(this.props)} */}
                    {user.firstname} <Icon type="down" />
                  </span>
                </Dropdown>
              </span>
              {/* <a href="/" onClick={() => this.props.logout()}>
                ออกจากระบบ
              </a> */}
            </React.Fragment>
          )}
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    { logout }
  )(Navbar)
);
