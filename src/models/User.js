import api from "./api";
import store from "./redux/store";
import { setCurrentUser } from "./redux/actions/authAction";
import decoder from "jwt-decode";

class User {
  constructor() {
    this.url = "/user";
  }

  getProfile = () => {
    return api.api_application.get(`${this.url}/profile/me`);
  };
  getOtherProfile = id => {
    return api.api_application.get(`${this.url}/profile/`, { params: { id } });
  };

  register = rawData => {
    const filters = [
      "type",
      "firstname",
      "lastname",
      "email",
      "password",
      "bankAccountNumber",
      "paymentType",
      "creditCardNumber",
      "cardExpireDate",
      "cvv"
    ];
    let data = {};
    if (rawData) {
      Object.keys(rawData).forEach(key => {
        if (filters.includes(key)) {
          Object.assign(data, { [key]: rawData[key] });
        }
      });
    }
    return api.api_application.post(`${this.url}/register`, data);
  };

  login = data => {
    return api.api_application.post(`${this.url}/login`, data);
  };

  searchPhotographer = q => {
    return api.api_application.get(`${this.url}/search-photographer`, {
      params: { q }
    });
  };

  update = rawData => {
    const filters = [
      "firstname",
      "lastname",
      "birthday",
      "phone",
      "facebook",
      "instagram",
      "lineId",
      "bio",
      "paymentType",
      "bankAccountNumber"
    ];
    const creditCardField = ["creditCardNumber", "cardExpireDate", "cvv"];
    let data = {};
    if (rawData) {
      Object.keys(rawData).forEach(e => {
        if (
          filters.includes(e) ||
          (rawData["paymentType"] === "credit card" &&
            creditCardField.includes(e))
        ) {
          Object.assign(data, { [e]: rawData[e] });
        }
      });
    }
    console.log("data: ", data);
    return api.api_application.post(`${this.url}/edit-profile`, data);
  };

  uploadProfilePhoto = data => {
    return api.api_multipart.post(`${this.url}/profile-photo`, data);
  };

  uploadCoverPhoto = data => {
    return api.api_multipart.post(`${this.url}/cover-photo`, data);
  };

  getEvents = () => {
    return api.api_application.get(`${this.url}/event`);
  };

  updateStoreUser = loggedInUser => {
    if (!localStorage.jwtToken) {
      return;
    }
    const decoded = decoder(localStorage.jwtToken);
    const oldLoggedInUser = JSON.parse(localStorage.getItem("LoggedInUser"));
    const updatedLoggedInUser = { ...oldLoggedInUser, ...loggedInUser };

    store.dispatch(
      setCurrentUser(localStorage.jwtToken, decoded, updatedLoggedInUser)
    );
    localStorage.setItem("LoggedInUser", JSON.stringify(updatedLoggedInUser));
  };

  /**
   * @param {string} password Input password to confirm delete account
   */
  deleteAccount = password => {
    return api.api_application.post(`${this.url}/delete`, {
      password: password
    });
  };
}

const UserApi = new User();
export default UserApi;
