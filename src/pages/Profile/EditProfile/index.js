import React from "react";
import EditPhotographerProfile from "../../../components/Profile/EditPhotographerProfile";
import EditUserProfile from "../../../components/Profile/EditUserProfile";
import UserApi from "../../../models/User";

class EditProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = null;
  }

  async componentDidMount() {
    const res = await UserApi.getProfile();
    this.setState({
      ...res.data
    });
  }

  render() {
    const { type } = { ...this.state };
    if (!this.state) {
      return <p>Loading...</p>;
    }
    return type === "photographer" ? (
      <EditPhotographerProfile profile={this.state} />
    ) : (
      <EditUserProfile profile={this.state} />
    );
  }
}

export default EditProfilePage;
