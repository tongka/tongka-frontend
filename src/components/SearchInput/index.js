import { Form, Row, Col, Input, Button, Cascader } from "antd";
import React, { Component } from "react";

import ServiceApi from "../../models/Service";
import UserApi from "../../models/User";

class SearchInput extends Component {
  state = {
    expand: false
  };

  handleSearch = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        let result = null;
        if (values.type[0] === "service") {
          result = await ServiceApi.search(values.q);
          console.log(result);
        } else if (values.type[0] === "photographer") {
          result = await UserApi.searchPhotographer(values.q);
        }
        if (result) this.props.onSearch(result.data, values.type[0]);
      } else {
        console.error(err);
        if (err.type) {
          alert(err.type.errors[0].message);
        }
      }
    });
  };

  handleReset = () => {
    this.props.form.resetFields();
  };

  toggle = () => {
    const { expand } = this.state;
    this.setState({ expand: !expand });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form
        onSubmit={this.handleSearch}
        style={{ margin: "0px auto 5px auto", maxWidth: "970px" }}
      >
        <Form.Item style={{ marginBottom: 0 }}>
          <Col sm={24} md={19} style={{ paddingRight: "5px" }}>
            {getFieldDecorator(`q`, {
              rules: [
                {
                  required: true,
                  message: "กรุณากรอกข้อความที่ต้องการค้นหา"
                }
              ]
            })(<Input placeholder="ค้นหา" />)}
          </Col>
          <Col sm={24} md={3} style={{ paddingRight: "5px" }}>
            {getFieldDecorator("type", {
              rules: [
                {
                  required: true,
                  message: "กรุณาเลือกประเภท"
                }
              ]
            })(
              <Cascader
                placeholder="ประเภท"
                options={[
                  {
                    value: "service",
                    label: "บริการ"
                  },
                  {
                    value: "photographer",
                    label: "ช่างภาพ"
                  }
                ]}
              />
            )}
          </Col>
          <Col sm={24} md={2}>
            <Button type="primary" htmlType="submit" style={{ width: "100%" }}>
              ค้นหา
            </Button>
          </Col>
        </Form.Item>
        {/* <a style={{ marginLeft: 8, fontSize: 12 }} onClick={this.toggle}>
            Filter <Icon type={this.state.expand ? "up" : "down"} />
          </a>
          {this.state.expand ? this.filter() : null} */}
      </Form>
    );
  }
  filter = () => (
    <Row>
      <Col span={8} style={{ display: "block" }}>
        <Form.Item>
          {this.props.form.getFieldDecorator(`filter-field`, {
            rules: [
              {
                required: true,
                message: "Input something!"
              }
            ]
          })(<Input placeholder="test filter field" />)}
        </Form.Item>
      </Col>
    </Row>
  );
}

export default Form.create({ name: "search" })(SearchInput);
