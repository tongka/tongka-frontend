import React from "react";
import { Row } from "antd";
import style from "./SearchItemService.module.css";
import { Link } from "react-router-dom";
import dayjs from "dayjs";
import svg_tag_1_grad from "../../../images/tags/tag_1_graduation.svg";
import svg_tag_2_port from "../../../images/tags/tag_2_portrait.svg";
import svg_tag_3_wedd from "../../../images/tags/tag_3_wedding.svg";
import svg_tag_4_even from "../../../images/tags/tag_4_event.svg";
import svg_tag_5_prew from "../../../images/tags/tag_5_prewedding.svg";
import svg_tag_6_arch from "../../../images/tags/tag_6_architech.svg";
import svg_tag_7_food from "../../../images/tags/tag_7_foodanddrink.svg";
import ImageApi from "../../../models/Image";

const svg_tag = [
  svg_tag_1_grad,
  svg_tag_2_port,
  svg_tag_3_wedd,
  svg_tag_4_even,
  svg_tag_5_prew,
  svg_tag_6_arch,
  svg_tag_7_food
];

class SearchItemService extends React.Component {
  render() {
    const {
      title,
      fullPrice,
      halfPrice,
      detail,
      startDate,
      endDate,
      owner,
      id,
      location,
      booltags,
      coverPhoto
    } = {
      ...this.props.data
    };
    return (
      <div className={style["result-area-container"]}>
        <div className={style["item-container"]}>
          <div className={style["item-image-container"]}>
            <Link to={`/service/${id}`}>
              <img
                className={style["item-image"]}
                src={ImageApi.getCoverServiceSrc(coverPhoto)}
                alt="service"
              />
            </Link>
          </div>

          <div className={style["item-detail"]}>
            <Link to={`/service/${id}`}>
              <h2>{title}</h2>
            </Link>
            <span className={style["item-detail-left-text"]}>
              ราคาครึ่งวัน: {halfPrice}
            </span>
            <span>ราคาเต็มวัน: {fullPrice}</span>
            <p>สถานที่: {location}</p>
            <p className={style["item-detail-description"]}>{detail}</p>
            <div className={style["tags-container"]}>
              {Object.keys(booltags).map(i => {
                return booltags[i] ? (
                  <img
                    src={svg_tag[i - 1]}
                    alt="tag symbol"
                    className={style["img-tag"]}
                  />
                ) : null;
              })}
            </div>
            <Row type="flex" justify="end" align="bottom">
              {owner} • {dayjs(startDate).format("DD/MM/YYYY")}-
              {dayjs(endDate).format("DD/MM/YYYY")}
            </Row>
          </div>
        </div>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="100%"
          height="1"
          version="1.1"
          style={{ margin: "0px", display: "block" }}
        >
          <rect width="100%" height="1" fill="#E0E0E0" />
        </svg>
      </div>
    );
  }
}

export default SearchItemService;
