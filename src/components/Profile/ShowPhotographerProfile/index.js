import { Link } from "react-router-dom";
import { Rate, Button, message } from "antd";
import React from "react";
import moment from "moment";
import store from "../../../models/redux/store";

import ImageApi from "../../../models/Image";

import MiniBox from "../../MiniBox";
import PortfolioCard from "../PortfolioCard";
import PortfolioApi from "../../../models/Portfolio";

import icon_edit from "../../../images/icons/edit.svg";
import icon_fb from "../../../images/icons/social_icon_facebook.svg";
import icon_ig from "../../../images/icons/social_icon_ig.svg";
import icon_line from "../../../images/icons/social_icon_line.svg";
import icon_tel from "../../../images/icons/social_icon_tel.svg";
import styles from "./ShowPhotographerProfile.module.css";

const lineBreaker = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="1"
    version="1.1"
    style={{ marginBottom: "1em", display: "block" }}
  >
    <rect width="100%" height="1" fill="#E0E0E0" />
  </svg>
);

const beforeDefaultUpload = function(file) {
  const isSupport = ["image/jpeg", "image/png", "image/tiff"].includes(
    file.type
  );
  if (!isSupport) {
    message.error("Not support file type");
  }
  const isLt5M = file.size / 1024 / 1024 < 5;
  if (!isLt5M) {
    message.error("Image must be smaller than 5MB!");
  }
  return isSupport && isLt5M;
};

class ShowPhotographerProfile extends React.Component {
  constructor(props) {
    super(props);
    this.currentUser = store.getState().auth.user;
    this.fetchPortfolio = this.fetchPortfolio.bind(this);
    this.handleChangeInputImage = this.handleChangeInputImage.bind(this);
    this.isPhotographerOwner = this.props.profile.id === this.currentUser.id;
    this.fetchPortfolio();
    this.inputFileRef = React.createRef();
    this.state = {
      portfolios: []
    };
  }

  fetchPortfolio = async () => {
    let res_portfolio = await PortfolioApi.getPortfolio(this.props.profile.id);
    this.setState({ portfolios: res_portfolio.data });
    console.log("fetching port");
    console.log(res_portfolio);
  };

  handleChangeInputImage = async e => {
    const files = e.target.files;
    if (files.length !== 1) {
      return;
    }
    if (!beforeDefaultUpload(files[0])) {
      return;
    }
    // alert("handling upload port " + files[0]);
    let formData = new FormData();
    formData.append("imageFile", files[0]);
    try {
      message.loading("กำลังอัปโหลด");
      const upload_res = await PortfolioApi.uploadPortfolio(formData);
      if (upload_res.status === 200) {
        message.success("อัปโหลดสำเร็จ");
        let tempPorts = this.state.portfolios;
        tempPorts.push(upload_res.data);
        this.setState({ portfolios: tempPorts });
      }
      return;
    } catch (err) {
      message.error("อัปโหลดล้มเหลว");
      console.log(err);
      return;
    }
  };

  handleClickUploadPortfolio = () => {
    this.inputFileRef.current.click();
  };

  handleDeletePortfolio = async photoId => {
    try {
      message.loading("กำลังลบ");
      await PortfolioApi.deletePortfolio(photoId);
      let tempPorts = this.state.portfolios.filter(val => {
        return val.id !== photoId;
      });
      this.setState({ portfolios: tempPorts });
      message.success("การลบสำเร็จ");
      return;
    } catch (err) {
      message.error("การลบล้มเหลว");
      console.log(err);
      return;
    }
  };

  render() {
    const { profile } = this.props;
    const joinedDate = profile.joinedDate
      ? moment(profile.joinedDate).format("DD/MM/YY")
      : "N/A";
    const birthday = profile.birthday
      ? moment(profile.birthday).format("DD/MM/YY")
      : "N/A";
    return (
      <div className={styles["encapsulator"]}>
        {" "}
        <div id={styles["cover-container"]}>
          <img
            id={styles["cover"]}
            src={ImageApi.getCoverSrc(profile.coverPhoto)}
            alt=""
          />
        </div>
        <div id={styles["content-container"]}>
          <img
            id={styles["avatar"]}
            src={ImageApi.getProfileSrc(profile.profilePhoto)}
            alt=""
          />
          <h1 id={styles["profileName"]}>
            {profile.firstname} {profile.lastname}
          </h1>
          <p className={styles["center-text"]}>{profile.bio}</p>
          <div className={styles["detail-box-container"]}>
            <MiniBox>{profile.eventCount} งาน</MiniBox>
            <MiniBox>
              <span style={{ marginLeft: "8px" }}>
                <Rate
                  className={styles["star-item"]}
                  defaultValue={profile.rating ? profile.rating : 0}
                  disabled
                />
              </span>
              <div style={{ display: "inline" }}>
                <span className={styles["flow-left"]}>
                  {profile.reviewCount} reviews
                </span>
                <span className={styles["flow-right"]}>
                  {profile.rating ? profile.rating : 0}/5.0
                </span>
              </div>
            </MiniBox>
            <MiniBox>
              <span>{joinedDate || "N/A"}</span>
              <span>Joined date</span>
            </MiniBox>
          </div>
          {lineBreaker}
          <div className={styles["about-title"]}>
            <h1 className={styles["text-title"]}>เกี่ยวกับ</h1>
            {!profile.isOther ? (
              <div id={styles["edit-button"]}>
                <Link to="/profile/edit">
                  <img src={icon_edit} alt="" sizes="25" />
                </Link>
              </div>
            ) : null}
          </div>
          <div className={styles["about"]}>
            <div id={styles["text-align-right"]}>
              <p>ชื่อจริง:</p>
              <p>นามสกุล:</p>
              <p>วันเกิด:</p>
              <p>เบอร์โทรศัพท์:</p>
            </div>
            <div id={styles["text-align-left"]}>
              <p>{profile.firstname}</p>
              <p>{profile.lastname}</p>
              <p>{birthday}</p>
              <p>{profile.phone ? profile.phone : "N/A"}</p>
            </div>
          </div>
          {lineBreaker}
          {/**This is basically social link */}
          <div
            className={styles["column-span"]}
            style={{
              marginBottom: `${
                this.state.portfolios.length >= 1 ? "1rem" : "5rem"
              }`
            }}
          >
            <span>
              <div className={styles["social-icon"]}>
                <img id={styles["cover"]} src={icon_fb} alt="" sizes="25" />
              </div>
              <div className={styles["social-info"]}>
                {profile.facebook ? profile.facebook : "N/A"}
              </div>
            </span>
            <span>
              <div className={styles["social-icon"]}>
                <img id={styles["cover"]} src={icon_ig} alt="" sizes="25" />
              </div>
              <div className={styles["social-info"]}>
                {profile.instagram ? profile.instagram : "N/A"}
              </div>
            </span>
            <span>
              <div className={styles["social-icon"]}>
                <img id={styles["cover"]} src={icon_tel} alt="" sizes="25" />
              </div>
              <div className={styles["social-info"]}>
                {profile.phone ? profile.phone : "N/A"}
              </div>
            </span>
            <span>
              <div className={styles["social-icon"]}>
                <img id={styles["cover"]} src={icon_line} alt="" sizes="25" />
              </div>
              <div className={styles["social-info"]}>
                {profile.lineId ? profile.lineId : "N/A"}
              </div>
            </span>
          </div>
          {/* portfolio goes here */}
          <input
            type="file"
            ref={this.inputFileRef}
            style={{ display: "none" }}
            onChange={this.handleChangeInputImage}
          />
          {this.state.portfolios.length >= 1 ? (
            <>
              {lineBreaker}
              <h1 className={styles["text-title"]}>ผลงาน</h1>
              <Button
                type="primary"
                shape="circle"
                icon="plus"
                className={styles["upload-port-button"]}
                onClick={this.handleClickUploadPortfolio}
                style={{
                  display: `${this.isPhotographerOwner ? "block" : "none"}`
                }}
              />
              <div
                id={styles["portfolio-cards-container"]}
                style={{
                  position: "relative",
                  top: `${this.isPhotographerOwner ? "-2.5em" : "0"}`
                }}
              >
                {this.state.portfolios.map(e => {
                  return (
                    <PortfolioCard
                      key={`portcard${e.id}`}
                      isPhotographerOwner={this.isPhotographerOwner}
                      {...e}
                      onDeletePort={this.handleDeletePortfolio}
                      cardWidth="100%"
                      cardHeight="auto"
                    />
                  );
                })}
              </div>
            </>
          ) : this.isPhotographerOwner ? (
            <>
              {lineBreaker}
              <h1 className={styles["text-title"]}>ผลงาน</h1>
              <Button
                type="primary"
                shape="circle"
                icon="plus"
                className={styles["upload-port-button"]}
                onClick={this.handleClickUploadPortfolio}
              />
            </>
          ) : null}
        </div>
      </div> /**Div contain everything */
    );
  }
}

export default ShowPhotographerProfile;
