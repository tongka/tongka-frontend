import { Link } from "react-router-dom";
import React from "react";
import moment from "moment";

import MiniBox from "../../MiniBox";

import ImageApi from "../../../models/Image";

import icon_edit from "../../../images/icons/edit.svg";
import icon_fb from "../../../images/icons/social_icon_facebook.svg";
import icon_ig from "../../../images/icons/social_icon_ig.svg";
import icon_line from "../../../images/icons/social_icon_line.svg";
import icon_tel from "../../../images/icons/social_icon_tel.svg";
import styles from "./ShowUserProfile.module.css";

const lineBreaker = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="1"
    version="1.1"
    style={{ marginBottom: "1em", display: "block" }}
  >
    <rect width="100%" height="1" fill="#E0E0E0" />
  </svg>
);
class ShowUserProfile extends React.Component {
  render() {
    const { profile } = this.props;
    const joinedDate = profile.joinedDate
      ? moment(profile.joinedDate).format("DD/MM/YY")
      : "N/A";
    const birthday = profile.birthday
      ? moment(profile.birthday).format("DD/MM/YY")
      : "N/A";
    return (
      <div className={styles["encapsulator"]}>
        {" "}
        <div id={styles["cover-container"]}>
          <img
            id={styles["cover"]}
            src={ImageApi.getCoverSrc(profile.coverPhoto)}
            alt=""
          />
        </div>
        <div id={styles["content-container"]}>
          <img
            id={styles["avatar"]}
            src={ImageApi.getProfileSrc(profile.profilePhoto)}
            alt=""
          />
          <h1 id={styles["profileName"]}>
            {profile.firstname} {profile.lastname}
          </h1>
          <p className={styles["center-text"]}>{profile.bio}</p>
          <div className={styles["detail-box-container"]}>
            <MiniBox>{profile.eventCount} งาน</MiniBox>
            <MiniBox>
              <span>{joinedDate || "N/A"}</span>
              <span>Joined date</span>
            </MiniBox>
          </div>
          {lineBreaker}
          <div className={styles["about-title"]}>
            <h1 className={styles["text-title"]}>เกี่ยวกับ</h1>
            {!profile.isOther ? (
              <div id={styles["edit-button"]}>
                <Link to="/profile/edit">
                  <img src={icon_edit} alt="" sizes="25" />
                </Link>
              </div>
            ) : null}
          </div>
          <div className={styles["about"]}>
            <div id={styles["text-align-right"]}>
              <p>ชื่อจริง:</p>
              <p>นามสกุล:</p>
              <p>วันเกิด:</p>
              <p>เบอร์โทรศัพท์:</p>
            </div>
            <div id={styles["text-align-left"]}>
              <p>{profile.firstname}</p>
              <p>{profile.lastname}</p>
              <p>{birthday}</p>
              <p>{profile.phone ? profile.phone : "N/A"}</p>
            </div>
          </div>
          {lineBreaker}
          {/**This is basically social link */}
          <div className={styles["column-span"]}>
            <span>
              <div className={styles["social-icon"]}>
                <img id={styles["cover"]} src={icon_fb} alt="" sizes="25" />
              </div>
              <div className={styles["social-info"]}>
                {profile.facebook ? profile.facebook : "N/A"}
              </div>
            </span>
            <span>
              <div className={styles["social-icon"]}>
                <img id={styles["cover"]} src={icon_ig} alt="" sizes="25" />
              </div>
              <div className={styles["social-info"]}>
                {profile.instagram ? profile.instagram : "N/A"}
              </div>
            </span>
            <span>
              <div className={styles["social-icon"]}>
                <img id={styles["cover"]} src={icon_tel} alt="" sizes="25" />
              </div>
              <div className={styles["social-info"]}>
                {profile.phone ? profile.phone : "N/A"}
              </div>
            </span>
            <span>
              <div className={styles["social-icon"]}>
                <img id={styles["cover"]} src={icon_line} alt="" sizes="25" />
              </div>
              <div className={styles["social-info"]}>
                {profile.lineId ? profile.lineId : "N/A"}
              </div>
            </span>
          </div>
        </div>
      </div> /**Div contain everything */
    );
  }
}

export default ShowUserProfile;
