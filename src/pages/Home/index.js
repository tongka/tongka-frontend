import React from "react";
import styles from "./Home.module.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Carousel, Button, Icon } from "antd";

import home_header from "../../images/home-header.jpg";
import banner2 from "../../images/banner-2.jpg";
import banner3 from "../../images/banner-3.jpg";

class Home extends React.Component {
  render() {
    return (
      <>
        <div className={styles["page--wrapper"]}>
          <div className={styles["top-ads"]}>
            <Carousel autoplay={true}>
              <div className={styles["top-ads--item"]}>
                <p
                  className={styles["top-ads--item--text"]}
                  style={{
                    top: "20%",
                    paddingLeft: "calc(100vw - (400px + 10vw))",
                    textShadow: "5px 5px 7px rgba(0,0,0,0.15)"
                  }}
                >
                  {`เพราะรูปถ่าย\nไม่ใช่ใครก็ถ่ายได้`}
                </p>
                {!this.props.auth.isAuth ? (
                  <div
                    style={{
                      position: "absolute",
                      top: "calc(20% + 7rem + 5rem)",
                      paddingLeft: "calc(100vw - (336px + 10vw))"
                    }}
                  >
                    <Link to="/register">
                      <Button
                        type="primary"
                        className={styles["top-ads--item--button"]}
                        style={{
                          width: "15rem",
                          height: "3rem"
                        }}
                      >
                        สมัครเลย
                      </Button>
                    </Link>
                  </div>
                ) : null}
                <img
                  src={home_header}
                  alt=""
                  className={styles["top-ads--item--image"]}
                />
              </div>
              <div className={styles["top-ads--item"]}>
                <p
                  className={styles["top-ads--item--text"]}
                  style={{
                    top: "20%",
                    paddingLeft: "calc(100vw - (400px + 10vw))",
                    textShadow: "5px 5px 7px rgba(0,0,0,0.15)"
                  }}
                >
                  {`รูปของคุณ\nคือความใส่ใจของเรา`}
                </p>
                {!this.props.auth.isAuth ? (
                  <div
                    style={{
                      position: "absolute",
                      top: "calc(20% + 7rem + 5rem)",
                      paddingLeft: "calc(100vw - (290px + 10vw))"
                    }}
                  >
                    <Link to="/register">
                      <Button
                        type="primary"
                        className={styles["top-ads--item--button"]}
                        style={{
                          width: "15rem",
                          height: "3rem"
                        }}
                      >
                        สมัครเลย
                      </Button>
                    </Link>
                  </div>
                ) : null}
                <img
                  src={banner2}
                  alt=""
                  className={styles["top-ads--item--image"]}
                />
              </div>
              <div className={styles["top-ads--item"]}>
                <p
                  className={styles["top-ads--item--text"]}
                  style={{
                    top: "10%",
                    paddingLeft: "calc(50vw - (360px))",
                    textShadow: "5px 5px 7px rgba(0,0,0,0.15)"
                  }}
                >
                  {`นึกถึงถ่ายภาพ\nนึกถึง TONGKA`}
                </p>
                {!this.props.auth.isAuth ? (
                  <div
                    style={{
                      position: "absolute",
                      top: "calc(10% + 7rem + 5rem)",
                      paddingLeft: "calc(49vw - (15rem + 30px))"
                    }}
                  >
                    <Link to="/register">
                      <Button
                        type="primary"
                        className={styles["top-ads--item--button"]}
                        style={{
                          width: "15rem",
                          height: "3rem"
                        }}
                      >
                        สมัครเลย
                      </Button>
                    </Link>
                  </div>
                ) : null}
                <img
                  src={banner3}
                  alt=""
                  className={styles["top-ads--item--image"]}
                />
              </div>
            </Carousel>
          </div>
          <div className={styles["mid-howto"]}>
            <h1>ขั้นตอนง่ายๆ ในการใช้บริการ</h1>
            <div className={styles["mid-howto--stpe-wrapper"]}>
              <div className={styles["mid-howto--stpe-wrapper--step"]}>
                <Icon
                  type="book"
                  theme="outlined"
                  className={styles["mid-howto--stpe-wrapper--step--icon"]}
                />
                <br />
                <span>1. เสนอช่างภาพ</span>
              </div>
              <div className={styles["mid-howto--stpe-wrapper--step"]}>
                <Icon
                  type="scan"
                  theme="outlined"
                  className={styles["mid-howto--stpe-wrapper--step--icon"]}
                />
                <br />
                <span>2. ชำระมัดจำ</span>
              </div>
              <div className={styles["mid-howto--stpe-wrapper--step"]}>
                <Icon
                  type="camera"
                  theme="outlined"
                  className={styles["mid-howto--stpe-wrapper--step--icon"]}
                />
                <br />
                <span>3. รับภาพ</span>
              </div>
              <div className={styles["mid-howto--stpe-wrapper--step"]}>
                <Icon
                  type="credit-card"
                  theme="outlined"
                  className={styles["mid-howto--stpe-wrapper--step--icon"]}
                />
                <br />
                <span>4. ชำระเงิน</span>
              </div>
            </div>
          </div>
        </div>
        <div className={styles["footer"]}>
          <span>Copyright © 2019 TONGKA</span>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({ auth: state.auth });

export default connect(
  mapStateToProps,
  null
)(Home);
