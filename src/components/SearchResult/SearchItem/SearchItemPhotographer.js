import React from "react";
import { Row, Col, Card, Rate } from "antd";
import style from "./SearchItemService.module.css";
import { Link } from "react-router-dom";
import ImageApi from "../../../models/Image";

class SearchItemPhotographer extends React.Component {
  render() {
    const {
      id,
      firstname,
      lastname,
      rating,
      reviewCount,
      eventCount,
      bio,
      profilePhoto
    } = {
      ...this.props.data
    };
    return (
      <div className={style["result-area-container"]}>
        <Card className={style["search-item-box"]}>
          <Row style={{ maxWidth: "900px" }}>
            <Col md={24} lg={6} style={{ paddingRight: "10px" }}>
              <img
                src={ImageApi.getProfileSrc(profilePhoto)}
                alt="avatar"
                style={{ width: "150px", borderRadius: "50%" }}
              />
            </Col>

            <Col md={24} lg={18}>
              <Link to={`/profile/${id}`}>
                <h2>
                  {firstname} {lastname}
                </h2>
              </Link>

              <p>
                <Rate disabled allowHalf defaultValue={rating ? rating : 0} />
                <h3 style={{ display: "inline" }}>{reviewCount} รีวิว</h3>
              </p>
              <p>
                <h3>{eventCount} งาน</h3>
              </p>
              <p>{bio}</p>
            </Col>
          </Row>
        </Card>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="100%"
          height="1"
          version="1.1"
          style={{ margin: "0px", display: "block" }}
        >
          <rect width="100%" height="1" fill="#E0E0E0" />
        </svg>
      </div>
    );
  }
}

export default SearchItemPhotographer;
