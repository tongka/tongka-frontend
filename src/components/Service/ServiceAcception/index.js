import React from "react";
import {
  Form,
  Cascader,
  Input,
  Row,
  Col,
  DatePicker,
  Button,
  Card
} from "antd";
import moment from "moment";
import User from "../../../models/User";
import ServiceAcceptionItem from "./ServiceAcceptionItem";

class ServiceAcception extends React.Component {
  handleSubmit = async e => {
    e.preventDefault();
    let data = null;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        data = { ...values, date: moment(values.date).format() };
      }
    });
    console.log(data);
    if (data) {
      window.location.href = window.location.href;
    }
    // if (data) {
    //   try {
    //     const res = await User.update(data);
    //     console.log(res);
    //     window.location.href = "/profile";
    //   } catch (e) {
    //     const err = e.response.data.reduce((out, val) => out.concat("\n", val));
    //     alert(err);
    //   }
    // }
  };

  render() {
    // console.log("sdds");
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        sm: { span: 24 },
        md: { span: 6 }
      },
      wrapperCol: {
        sm: { span: 24 },
        md: { span: 18 }
      }
    };

    const tailFormItemLayout = {
      wrapperCol: {
        sm: {
          span: 24,
          offset: 0
        },
        md: {
          span: 12,
          offset: 6
        }
      }
    };

    return (
      <>
        <p style={{ textAlign: "center", fontWeight: "bold" }}>
          ข้อเสนอที่ได้รับ
        </p>
        <ServiceAcceptionItem />
      </>
    );
  }
}

export default Form.create({ name: "ServiceAcception" })(ServiceAcception);
