import api from "./api";

class Event {
  constructor() {
    this.url = "/event";
  }

  confirm = (eventId, acceptanceStatus) => {
    // 0 -> 1
    const data = {
      id: String(eventId),
      accept: String(acceptanceStatus)
    };
    return api.api_application.post(`${this.url}/confirm`, data);
  };

  pay_30 = formData => {
    // 1 -> 2
    return api.api_multipart.post(`${this.url}/pay-30`, formData);
  };

  accept_30 = eventId => {
    // 2 -> 3
    const data = {
      id: String(eventId)
    };
    return api.api_application.post(`${this.url}/accept-30`, data);
  };

  deliver_photo = (eventId, photoUrlLink) => {
    // 3 -> 4
    const data = {
      id: String(eventId),
      photoLink: String(photoUrlLink)
    };
    return api.api_application.post(`${this.url}/deliver-photo`, data);
  };

  pay_100 = formData => {
    // 4 -> 5
    return api.api_multipart.post(`${this.url}/pay-100`, formData);
  };

  accept_100 = eventId => {
    // 5 -> 6
    const data = {
      id: String(eventId)
    };
    return api.api_application.post(`${this.url}/accept-100`, data);
  };

  rate = (eventId, star) => {
    const data = {
      id: String(eventId),
      score: String(star)
    };
    return api.api_application.post(`${this.url}/rate`, data);
  };

  cancel = eventId => {
    const data = {
      id: String(eventId)
    };
    return api.api_application.post(`${this.url}/cancel`, data);
  };

  pay_redeem = formData => {
    // 7 -> 9
    return api.api_multipart.post(`${this.url}/pay-redeem`, formData);
  };

  accept_redeem = eventId => {
    // 9 -> 10
    const data = {
      id: String(eventId)
    };
    return api.api_application.post(`${this.url}/accept-redeem`, data);
  };
}

const EventApi = new Event();
export default EventApi;
