import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import store from "./models/redux/store";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import decoder from "jwt-decode";
import { setCurrentUser } from "./models/redux/actions/authAction";

if (localStorage.jwtToken) {
  const decoded = decoder(localStorage.jwtToken);
  const loggedInUser = JSON.parse(localStorage.getItem("LoggedInUser"));
  store.dispatch(setCurrentUser(localStorage.jwtToken, decoded, loggedInUser));
}

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
