import React from "react";
import { Button, Modal, Radio } from "antd";
import styles from "./PaymentConfirmation.module.css";
import ImageUploader from "../ImageUploader";

class PaymentConfirmation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPayVis: false,
      selectedPayType: "bank account",
      imageSlipFile: null
    };
    this.handlePay = this.props.onPay;
    this.handleShowPaymentPage = this.handleShowPaymentPage.bind(this);
    this.handleHidePaymentPage = this.handleHidePaymentPage.bind(this);
    this.handleSelectedPayType = this.handleSelectedPayType.bind(this);
    this.handleSlipUpload = this.handleSlipUpload.bind(this);
    this.handleClickSubmit = this.handleClickSubmit.bind(this);
  }

  handleShowPaymentPage = () => {
    this.setState({
      isPayVis: true
    });
  };

  handleHidePaymentPage = () => {
    this.setState({ isPayVis: false });
  };

  handleSelectedPayType = e => {
    this.setState({ selectedPayType: e.target.value });
  };

  handleSlipUpload = async function(file) {
    const thefile = file;
    this.setState({ imageSlipFile: thefile });
    return { result: true, filepath: URL.createObjectURL(thefile) };
  };

  handleClickSubmit = async function() {
    const payRes = await this.handlePay(
      this.state.selectedPayType,
      this.state.imageSlipFile
    );
    if (payRes === true) {
      this.setState({ isPayVis: false });
    }
  };

  render() {
    const {
      isDisabled,
      bankAccountNumber,
      isCreditCardAvailiable,
      creditCardNumber,
      cardExpireDate,
      stateNo,
      price,
      isPhotographerOwner
      //   cvv
    } = this.props;
    const button_text = isPhotographerOwner
      ? "คืนเงิน"
      : [1, 2].includes(stateNo)
      ? "ชำระมัดจำ"
      : "ชำระเงิน";
    // const pay_price = [1, 2].includes(stateNo)
    //   ? (price * 0.3).toFixed(2)
    //   : (price * 0.7).toFixed(2);
    return (
      <>
        <Button
          type="primary"
          className={styles["button"]}
          onClick={this.handleShowPaymentPage}
          disabled={isDisabled}
        >
          {button_text}
        </Button>
        <Modal
          visible={this.state.isPayVis}
          title="เลือกช่องทางการชำระเงิน"
          onOk={this.handleClickSubmit}
          onCancel={this.handleHidePaymentPage}
          footer={[
            <Button key="back" onClick={this.handleHidePaymentPage}>
              กลับ
            </Button>,
            <Button
              key="submit-pay"
              type="primary"
              onClick={this.handleClickSubmit}
            >
              {button_text}
            </Button>
          ]}
        >
          <Radio.Group
            value={this.state.selectedPayType}
            onChange={this.handleSelectedPayType}
          >
            <Radio.Button value="bank account">บัญชีธนาคาร</Radio.Button>
            <Radio.Button
              value="credit card"
              disabled={isCreditCardAvailiable === 0}
            >
              บัตรเครดิต
            </Radio.Button>
          </Radio.Group>
          <div style={{ margin: "0.5rem auto" }}>
            {this.state.selectedPayType === "bank account" ? (
              <>
                <h3>ช่องทางบัญชีธนาคาร</h3>
                <p>{`ราคาที่ต้องชำระ: ${price}`}</p>
                <p>{`เลขที่บัญชี: ${bankAccountNumber}`}</p>
                <h3>หลักฐานการชำระเงิน</h3>
                <div
                  style={{
                    width: "200px",
                    height: "200px"
                  }}
                >
                  <ImageUploader
                    onUpload={this.handleSlipUpload}
                    beforeUpload={this.beforeSlipUpload}
                    imageUrl={null}
                    width="200px"
                    height="200px"
                    imgHeight="200px"
                    imgWidth="100%"
                    localImage={true}
                  />
                </div>
              </>
            ) : (
              <>
                <h3>ช่องทางบัตรเครดิต</h3>
                <p>{`ราคาที่ต้องชำระ: ${price}`}</p>
                <p>{`เลขที่บัตรเครดิต: ${creditCardNumber}`}</p>
                <p>{`วันที่หมดอายุ: ${cardExpireDate}`}</p>
              </>
            )}
          </div>
        </Modal>
      </>
    );
  }
}

export default PaymentConfirmation;
