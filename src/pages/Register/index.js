import React from "react";

import {
  Form,
  Input,
  Checkbox,
  Button,
  Row,
  Col,
  Radio,
  DatePicker,
  Modal
} from "antd";
import RadioGroup from "antd/lib/radio/group";
import moment from "moment";

import UserTypeSelector from "../../components/UserTypeSelector";
import ClickableText from "../../components/ClickableText";

import UserApi from "../../models/User";
// import UploadProfile from '../../components/UploadProfile'

class RegistrationForm extends React.Component {
  constructor(props) {
    super(props);
    this.UserTypeSelector_instance = React.createRef();
  }
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    imgUrl: null,
    paymentType: "bank account"
  };

  handleTermOfUseModal = e => {
    e.preventDefault();
    Modal.info({
      title: "ข้อตกลงการใช้งาน",
      maskClosable: true,
      content: <div>ข้อตกลงการใช้งาน</div>
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    let data = null;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        data = {
          ...values,
          type: this.UserTypeSelector_instance.current.state.usertype.value,
          cardExpireDate: moment(values.cardExpireDate).format()
        };
      }
    });
    if (data) {
      try {
        const res = await UserApi.register(data);
        console.log(res);
        window.location.href = "/login";
      } catch (e) {
        const err = e.response.data.reduce((out, val) => out.concat("\n", val));
        Modal.error({
          title: "Tonka: Error message",
          content: err
        });
      }
    }
  };

  handleUploadProfile = imgUrl => {
    this.setState({ ...this.state, imgUrl });
  };

  handleOnPaymentTypeChange = e => {
    this.setState({ paymentType: e.target.value });
  };

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 12,
          offset: 8
        }
      }
    };

    return (
      <div style={{ width: "100%" }}>
        <div>
          <h1 align="center">TONGKA ยินดีต้อนรับ</h1>
        </div>
        <div style={{ width: "100%" }}>
          <UserTypeSelector ref={this.UserTypeSelector_instance} />
        </div>
        <Form onSubmit={this.handleSubmit}>
          <Row gutter={24}>
            <Col md={{ span: 24, offset: 2 }} />
          </Row>
          <Form.Item {...formItemLayout} label="อีเมล">
            {getFieldDecorator("email", {
              rules: [
                {
                  type: "email",
                  message: "อีเมลไม่ถูกต้อง"
                },
                {
                  required: true,
                  message: "กรุณากรอกอีเมล"
                }
              ]
            })(<Input placeholder="อีเมลแอดเดรส" />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="รหัสผ่าน">
            {getFieldDecorator("password", {
              rules: [
                {
                  required: true,
                  pattern: /(?=.{8,})/,
                  message:
                    "รหัสผ่านจะต้องมีความยาวมากกว่าหรือเท่ากับ 8 ตัวอักษร"
                },
                {
                  validator: this.validateToNextPassword
                }
              ]
            })(<Input type="password" placeholder="รหัสผ่าน" />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="กรอกรหัสผ่านอีกครั้ง">
            {getFieldDecorator("confirm", {
              rules: [
                {
                  required: true,
                  message: "กรุณายืนยันรหัสผ่าน"
                },
                {
                  validator: this.compareToFirstPassword
                }
              ]
            })(
              <Input
                type="password"
                onBlur={this.handleConfirmBlur}
                placeholder="กรอกรหัสผ่านอีกครั้ง"
              />
            )}
          </Form.Item>
          {/* <UploadProfile handleUpload={this.handleUploadProfile} /> */}
          <Form.Item {...formItemLayout} label="ชื่อ">
            {getFieldDecorator("firstname", {
              rules: [
                {
                  required: true,
                  message: "กรุณาระบุชื่อ"
                }
              ]
            })(<Input placeholder="ชื่อ" />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="นามสกุล">
            {getFieldDecorator("lastname", {
              rules: [
                {
                  required: true,
                  message: "กรุณาระบุนามสกุล"
                }
              ]
            })(<Input placeholder="นามสกุล" />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="เลขที่บัญชี">
            {getFieldDecorator("bankAccountNumber", {
              rules: [
                {
                  required: true,
                  message: "กรุณาระบุเลขที่บัญชี"
                }
              ]
            })(
              <Input placeholder="เลขที่บัญชีของท่าน เช่น 111-11122-23365-11" />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="ประเภทการชำระเงิน">
            {getFieldDecorator("paymentType", { initialValue: "bank account" })(
              <RadioGroup
                name="radiogroup_paymentType"
                onChange={this.handleOnPaymentTypeChange}
              >
                <Radio value={"bank account"}>โอนผ่านธนาคาร</Radio>
                <Radio value={"credit card"}>จ่ายผ่านบัตรเครดิต</Radio>
              </RadioGroup>
            )}
          </Form.Item>
          {this.state.paymentType === "bank account" ? null : (
            <>
              <Form.Item {...formItemLayout} label="เลขบัตรเครดิต">
                {getFieldDecorator("creditCardNumber", {
                  rules: [
                    {
                      required: true,
                      message: "กรุณาระบุเลขบัตรเครดิต"
                    }
                  ]
                })(
                  <Input placeholder="เลขบัตรเครดิตของท่าน เช่น 111-11122-23365-11" />
                )}
              </Form.Item>
              <Form.Item {...formItemLayout} label="วันที่บัตรหมดอายุ">
                {getFieldDecorator("cardExpireDate", {
                  rules: [
                    {
                      required: true,
                      message: "กรุณาระบุวันหมดอายุของบัตรเครดิต"
                    }
                  ]
                })(<DatePicker format="DD/MM/YYYY" />)}
              </Form.Item>
              <Form.Item {...formItemLayout} label="เลข cvv">
                {getFieldDecorator("cvv", {
                  rules: [
                    {
                      required: true,
                      message: "กรุณาระบุเลข cvv"
                    }
                  ]
                })(<Input placeholder="ตัวเลข cvv ด้านหลังบัตร" />)}
              </Form.Item>
            </>
          )}

          <Form.Item {...tailFormItemLayout}>
            {getFieldDecorator("agreement", {
              valuePropName: "checked",
              initialValue: false,
              rules: [
                {
                  required: true,
                  transform: value => value || undefined,
                  type: "boolean",
                  message: "กรุณายอมรับข้อตกลงการใช้งาน"
                }
              ]
            })(
              <Checkbox>
                {/* TODO: Add term of use */}
                ยอมรับ{" "}
                <ClickableText onClick={this.handleTermOfUseModal}>
                  ข้อตกลงการใช้งาน
                </ClickableText>
              </Checkbox>
            )}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit" block>
              สมัครสมาชิก
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default Form.create({ name: "register" })(RegistrationForm);
