import React from "react";

// import styles from "./Search.module.css";
import SearchInput from "../../components/SearchInput";
import SearchResult from "../../components/SearchResult";
// import SearchItemPhotographer from '../../components/SearchResult/SearchItem/SearchItemPhotographer'

class Search extends React.Component {
  state = {
    search_result: null,
    type: ""
  };
  getSearchData = (data, type) => {
    this.setState({ search_result: data, type });
  };
  render() {
    return (
      <>
        <SearchInput
          onSearch={(data, type) => this.getSearchData(data, type)}
        />
        <SearchResult data={this.state.search_result} type={this.state.type} />
      </>
    );
  }
}

export default Search;
