import React from "react";
import ShowUserProfile from "../../components/Profile/ShowUserProfile";
import ShowPhotographerProfile from "../../components/Profile/ShowPhotographerProfile";
import UserApi from "../../models/User";

class ProfilePage extends React.Component {
  state = {};

  componentWillUpdate(nextProps, nextState) {
    if (this.props === nextProps || this.state === nextState) return;
    this.fetchNewProfile(this.props.match.params.id);
  }
  fetchNewProfile = async pId => {
    let res;
    if (pId) {
      res = await UserApi.getOtherProfile(pId);
    } else {
      res = await UserApi.getProfile();
    }

    this.setState({
      ...res.data,
      isOther: pId ? 1 : 0
    });
  };
  componentDidMount() {
    this.fetchNewProfile(this.props.match.params.id);
  }

  render() {
    const { type } = { ...this.state };
    if (!this.state) {
      return <p>Loading...</p>;
    }
    return type === "photographer" ? (
      <ShowPhotographerProfile profile={this.state} />
    ) : (
      <ShowUserProfile profile={this.state} />
    );
  }
}

export default ProfilePage;
