import React, { Component } from "react";

import SearchItemService from "../../SearchResult/SearchItem/SearchItemService";

class ServiceResult extends Component {
  state = {
    result: null
  };
  componentWillUpdate(nextProps, nextState) {
    if (this.state === nextState || this.props === nextProps) return;
  }

  render() {
    let items = null;
    const { data } = this.props;
    if (data) {
      items = data.map(e => {
        const {
          id,
          title,
          owner,
          halfDayPrice,
          fullDayPrice,
          description,
          openDate,
          closeDate,
          location,
          tags,
          coverPhoto
        } = e;
        let booltags = {
          1: false,
          2: false,
          3: false,
          4: false,
          5: false,
          6: false,
          7: false
        };
        tags.forEach(tagpair => {
          if (tagpair.id !== 8) {
            booltags[tagpair.id] = true;
          }
        });
        return (
          <SearchItemService
            key={id}
            data={{
              title,
              coverPhoto,
              owner,
              halfPrice: halfDayPrice,
              fullPrice: fullDayPrice,
              detail: description,
              startDate: openDate,
              endDate: closeDate,
              location: location,
              tags: tags,
              booltags: booltags,
              id
            }}
          />
        );
      });
    }

    return <>{items}</>;
  }
}
export default ServiceResult;
